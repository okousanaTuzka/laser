import signal
import time
import curses
import math
import subprocess
import argparse
from gpiozero import Button, DigitalInputDevice, MCP3208
from curses import wrapper
from queue import Queue, Empty
from threading import Thread
from collections import deque

from pylib.utils import CycleQueue


class GLOBAL(object):
    time_ = None
    state = False


def process_state(input_device):
    if input_device.value == 1:
        print("on")
        GLOBAL.time_ = time.perf_counter()
    else:
        print("off")
        print(time.perf_counter() - GLOBAL.time_, "s")
        GLOBAL.time_


class Panel(object):
    def __init__(self, y, x, h, w):
        self.canvas = curses.newpad(h, w)
        self.y, self.x = y, x
        self.h, self.w = h, w


class Label(Panel):
    def __init__(self, y, x, h, w, text=""):
        super(Label, self).__init__(y, x, h, w)
        self.text = text

    def set_text(self, text):
        self.text = text

    def repaint(self):
        self.canvas.clear()
        self.canvas.addstr(self.text)
        self.canvas.move(0,0)
        self.canvas.refresh(0, 0, self.y, self.x, self.y + self.h, self.x + self.w)
    

class BinaryChart(Panel):
    def __init__(self, y, x, h, w):
        super(BinaryChart, self).__init__(y, x, h, w)
        self.queue = CycleQueue(self.w, "_")

    def put(self, value):
        self.queue.put({True: '-', False: '_'}[bool(value)])

    def repaint(self):
        self.canvas.addstr(0, 0, "".join(self.queue.values()))
        self.canvas.addstr(0, self.queue.get_index(), "|")
        self.canvas.move(0,0)
        self.canvas.refresh(0, 0, self.y, self.x, self.y + self.h, self.x + self.w)
        

class HeightChart(Panel):
    LEVELS = "\u23bd\u23bc\u2500\u23bb\u23ba"
    def __init__(self, y, x, h, w):
        super(HeightChart, self).__init__(y, x, h, w)
        self.x_offset = 6
        self.gw = self.w - self.x_offset
        self.queue = deque([None] * self.gw)
        display_resolution = self.h * len(HeightChart.LEVELS)
        self.unit_size = 4096 // display_resolution

    def put(self, value):
        self.queue.append(value)
        self.queue.popleft()

    def repaint(self):
        self.canvas.erase()
        #if len(list(filter(lambda v: v is not None, self.queue))) > 1:
        if  len(self.queue)> 1:
            #max_ = max(*filter(lambda v: v is not None, self.queue))
            #min_ = min(*filter(lambda v: v is not None, self.queue))
            max_ = 4096
            min_ = 0
            delta = 4096
            self.paint_axis(min_, max_)
            for i in range(len(self.queue)):
                value = self.queue[i]
                if value is None:
                    continue
                units = value // self.unit_size
                position_y = self.h - units // len(HeightChart.LEVELS) - 1
                position_y = min(position_y, self.h - 1)
                level = int(units % len(HeightChart.LEVELS))
                try:
                    self.canvas.addstr(position_y, i + self.x_offset, HeightChart.LEVELS[level])
                except BaseException:
                    print("'{}-{}'".format(position_y, i))
                
        self.canvas.move(0,0)
        self.canvas.refresh(0,0, self.y, self.x,self.y + self.h, self.x + self.w)

    def paint_axis(self, min_, max_):
        for i in range(0, self.h):
            self.canvas.addstr(i, 0, "{}|".format(" " * (self.x_offset - 1)))
        
        y_step = 4
        for i in range(y_step, self.h):
            if self.h - 1 % i == 0:
                y_step = i
                break
        parts = self.h // y_step
        value_step = (max_ - min_) // parts
        value = max_
        for y in range(0, self.h, y_step):
            if value == max_:
                self.canvas.addstr(y, 0, "{:+5d}{}".format(value, HeightChart.LEVELS[4]))
            else:
                self.canvas.addstr(y, 0, "{:+5d}{}".format(value, HeightChart.LEVELS[2]))
            value -= value_step
        self.canvas.addstr(self.h - 1, 0, "{:+5d}{}".format(min_, HeightChart.LEVELS[0]))


class Stage(object):
    def __init__(self):
        self.panels = []

    def add(self, panel):
        self.panels.append(panel)

    def repaint(self):
        for panel in self.panels:
            panel.repaint()


def run_async(callable_):
    t = Thread(target=callable_, daemon=True)
    t.start()


def create_connector(adc_out, output_queue):
    def connector():
        while True:
            v = int.from_bytes(adc_out.read(2), "little")
            output_queue.put(v)
    return connector


class Main(object):

    def __init__(self, sample_rate):
        self.sample_rate = sample_rate
        self.stdscr = None
        self.senzor = None
        self.stage = None
        self.chart = None
        self.queue = Queue(20000)

    def init(self, stdscr):
        curses.curs_set(0)
        self.stdscr = stdscr
        self.stage = Stage()
        
        self.stdscr.addstr("LED light:")
        self.chart = BinaryChart(1, 0, 2, 100)
        self.stage.add(self.chart)

        chart2 = HeightChart(3,0,41, stdscr.getmaxyx()[1])
        self.stage.add(chart2)

        label = Label(44, 1, 1, 50)
        self.stage.add(label)

        label2 = Label(45, 1, 1, 50)
        self.stage.add(label2)

        self.stage.repaint()

        self.senzor = DigitalInputDevice(17, pull_up=False)
        self.senzor.when_activated = lambda: self.chart.put(True)
        self.senzor.when_deactivated = lambda: self.chart.put(False)

        self.stdscr.refresh()
        print("nic")
        
        self.adc = subprocess.Popen(["sudo", "/home/pi/david/c/core", "--sample_rate", str(self.sample_rate)], stdout=subprocess.PIPE, bufsize=100)
        run_async(create_connector(self.adc.stdout, self.queue))
        counter = 0
        repaint_counter = 0
        st = time.time()
        t = time.perf_counter()
        batch_size = 20
        while True:
            buff_size = self.queue.qsize()
            if counter > 100:
                now = time.time()
                label2.set_text("sample rate: {} s/s, repaints: {} r/s".format(counter // (now - st), repaint_counter // (now - st)))
                st = now
                counter = 0
                repaint_counter = 0
                if buff_size > batch_size * 12 and batch_size < 1000:
                    batch_size = int(batch_size * 1.3)
                if buff_size * 10  < batch_size and batch_size > 10:
                    batch_size = int(batch_size / 1.1)
            label.set_text("buffer: {}".format(buff_size))
            try:
                for i in range(batch_size):
                    v = self.queue.get(block=False)
                    chart2.put(v)
                    counter += 1
                    #time.sleep(0.001)
            except Empty:
                pass
            self.stage.repaint()
            repaint_counter += 1
            now = time.perf_counter()
            x = round(0.05 - (now - t), 3)
            if x > 0.001:
                print("limiting frames")
                time.sleep(x)
            t = now
        #signal.pause()

    def close(self):
        self.adc.close()

parser = argparse.ArgumentParser()
parser.add_argument("--sample_rate", default=300)
args = parser.parse_args()


main = Main(args.sample_rate)
wrapper(main.init)
main.close()

