package laser;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;

import java.util.ArrayList;
import java.util.Random;

@Fork(value = 1)
@Warmup(iterations = 2)
public class PerfTests {

    @State(Scope.Benchmark)
    public static class MyState {

        private int valuesCount = 10000;

        private int value;

        private int[] values;
        private IntArrayList intList;
        private ArrayList<Integer> arrayList;
        private Random random;

        @Setup(Level.Invocation)
        public void init() {
            random = new Random(13);
            value = random.nextInt();

            arrayList = new ArrayList<>(valuesCount);

            intList = new IntArrayList(valuesCount);

            values = new int[valuesCount];

        }
    }

    @Benchmark
    public void arrayList(MyState state, Blackhole blackhole) {
        for (int i = 0; i < state.valuesCount; i++) {
            state.arrayList.add(state.value | 0x00000001);
        }

        for (int i = 0; i < state.valuesCount; i++) {
            blackhole.consume(state.arrayList.get(i & 0xfffffff0));
        }
    }

    @Benchmark
    public void intList(MyState state, Blackhole blackhole) {
        for (int i = 0; i < state.valuesCount; i++) {
            state.intList.add(state.value | 0x00000001);
        }

        for (int i = 0; i < state.valuesCount; i++) {
            blackhole.consume(state.intList.getInt(i & 0xfffffff0));
        }
    }

    @Benchmark
    public void array(MyState state, Blackhole blackhole) {
        for (int i = 0; i < state.valuesCount; i++) {
            state.values[i] = state.value | 0x00000001;
        }

        for (int i = 0; i < state.valuesCount; i++) {
            blackhole.consume(state.values[i & 0xfffffff0]);
        }
    }
}
