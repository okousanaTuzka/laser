package laser.helpers;

import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.*;

public class SignalAnalyzerTest {

    @Test
    public void testPutValue() {

        SignalAnalyzer analyzer = new SignalAnalyzer(100, 200);

        assertEquals(putAll(analyzer, 2, 2, 2, 2, 2), null);
        assertEquals(putAll(analyzer, 2, 200, 2, 200, 2, 1, 1, 1, 1), Arrays.asList(1, 1, 1));
        assertEquals(putAll(analyzer, 2, 101, 2, 101, 2, 1, 1, 1, 1), null);
        assertEquals(putAll(analyzer, 2, 200, 200, 200, 1, 1, 1, 50, 120, 2, 1, 1, 1, 1), Arrays.asList(3, 4, 1));
        assertEquals(putAll(analyzer, 2, 50, 120, 120, 120, 1, 1, 1, 1, 200, 2, 1, 1, 1, 1), Arrays.asList(3, 4, 1));

    }


    private List<Integer> putAll(SignalAnalyzer analyzer, int ... values) {
        List<Integer> result = null;
        for (int value : values) {
            List<Integer> r = analyzer.putValue(value);
            if (r != null) {
                assertNull(result);
                result = r;
            }
        }
        return result;
    }
}