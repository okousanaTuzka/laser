package laser;

import org.testng.annotations.Test;

import java.time.Duration;
import java.util.Arrays;

import static org.testng.Assert.*;

public class UtilsTest {

    @Test
    public void testEncode() {
        assertEquals(Utils.encode(2, 3, Duration.ofMillis(1), 1000), new int[] {3050, 1050, 1050, 3050, 1050, 1050, 3050});
        assertEquals(Utils.encode(2, 3, Duration.ofMillis(50), 1000).length, 25);
    }

    @Test
    public void testDecode() {
        assertEquals(Utils.decode(Arrays.asList(4, 1, 2, 1, 2, 3, 3), 3), Integer.valueOf(4));
        assertEquals(Utils.decode(Arrays.asList(4, 1, 2, 1, 2, 3, 3), 2), null);

        assertEquals(Utils.decode(Arrays.asList(2, 4, 4, 1, 2, 4, 4), 2), Integer.valueOf(2));
        assertEquals(Utils.decode(Arrays.asList(2, 4, 4, 1, 2, 4, 4), 1), null);
    }
}