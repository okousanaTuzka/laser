package laser;

import org.slf4j.Logger;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Utils {

    public static void Assert(Boolean bool) {
        Assert(bool, "");
    }

    /**
     * @param msg message text, will be formatted same way as String.format()
     * @param args arguments for formatting marks in message
     */
    public static void Assert(Boolean bool, String msg, Object ... args) {
        if (!bool) throw new IllegalStateException(String.format(msg, args));
    }

    public interface Action {
        void call() throws Exception;
    }

    /**
     * execute code and if fail write to error log exception and massage
     * return true if code is successfully executed, false in case of any exception
     */
    public static boolean tryAndLogErr(Action fnc, Logger logger, String message, Object ... args) {
        try {
            fnc.call();
            return true;
        } catch (Exception e) {
            logger.error(String.format(message, args), e);
            return false;
        }
    }

    public static byte[] toLittleEndian(int ... values) {
        byte[] bytes = new byte[values.length * 4];
        int byteIndex = 0;
        for (int value : values) {
            bytes[byteIndex++] = ((byte) value);
            bytes[byteIndex++] = ((byte) (value >>> 8));
            bytes[byteIndex++] = ((byte) (value >>> 16));
            bytes[byteIndex++] = ((byte) (value >>> 24));
        }
        return bytes;
    }

    /**
     *  if sample time is longer than 1/2 of short impulse(logical 0) and shorter than short impulse then there will
     *  be always min one sample within pulse and max 2 samples within pulse
     *  T0 - short impulse time
     *  T1 - long impulse time
     *  Ts - sample time (time between samples)
     *  (T0 / 2) < Ts < T0
     *
     *  if sample time is shorter then 1/3 of long impulse(logical 1) than there will be always min three samples
     *  within pulse and max four
     *  Ts < (T1 / 3)
     *
     *  with this 1 or 2 samples mean logical 0 and 3 or 4 samples mean logical 1
     *  most performance efficient setup (allowing lowest sample rate) is close to limit Ts < T0
     *  and for keep shortest signal set long impulse close to limit Ts < (T1 / 3)
     *
     */
    public static Duration getShortPulseTime(int sampleRate) {
        return Duration.ofNanos((1_000_000_000 / sampleRate) + 50_000);
    }

    public static Duration getLongPulseTime(int sampleRate) {
        return Duration.ofNanos((1_000_000_000 / sampleRate) * 3 + 50_000);
    }


    /**
     * create signal times
     */
    // TODO: implement signal length "stabilization"
    public static int[] encode(int playerId, int encodedBits, Duration shootTime, int sampleRate) {
        int shortPulse = (int) TimeUnit.NANOSECONDS.toMicros(Utils.getShortPulseTime(sampleRate).getNano());
        int longPulse  = (int) TimeUnit.NANOSECONDS.toMicros(Utils.getLongPulseTime(sampleRate).getNano());
        int shootLenght  = (int) TimeUnit.NANOSECONDS.toMicros(shootTime.getNano());

        int singleSignalLenght = 0;
        ArrayList<Integer> singleSignal = new ArrayList<>();

        // add signal separator (indicate single number begin)
        singleSignal.add(longPulse);
        singleSignalLenght += longPulse;

        for (int bit = 0; bit < encodedBits; bit++) {
            if ((playerId & 1 << bit) > 0) {
                singleSignal.add(longPulse);
                singleSignalLenght += longPulse;
            } else {
                singleSignal.add(shortPulse);
                singleSignalLenght += shortPulse;
            }
            // add bit separator
            if (bit != encodedBits -1) {
                // last bit in signal not need bit separator
                singleSignal.add(shortPulse);
                singleSignalLenght += shortPulse;
            }
        }

        int repeat = Math.max(1, shootLenght / singleSignalLenght);

        int[] result = new int[repeat * singleSignal.size() + 1];
        for (int i = 0; i < result.length -1; i++) {
            result[i] = singleSignal.get(i % singleSignal.size());
        }
        // signal should start and end with signal separator
        result[result.length -1] = longPulse;

        return result;
    }

    /**
     *
     * @param signal array contains received signal, each value should be pulse length in amount of samples
     * @param encodedBits how many bits should encoded number have
     * @return decoded number or null if decoding is unsuccessful
     */
    public static Integer decode(List<Integer> signal, int encodedBits) {
        int result = 0;

        boolean haveBegin = false;
        boolean separator = true;
        int bit = 0;

        for (int v : signal) {
            if (!haveBegin && separator) {
                if (v > 2) {
                    haveBegin = true;
                }
            }
            if (haveBegin && !separator && v > 2) {
                // logic 1
                result |= 1 << bit;
            } // else logic 0 = nothing to do
            if (haveBegin && !separator) {
                bit++;
            }
            if (haveBegin && separator && v > 2) {
                if (bit == encodedBits) {
                    return result;
                } else {
                    // reset
                    result = 0;
                    bit = 0;
                }
            }
            separator = !separator;
        }
        return null;
    }


}
