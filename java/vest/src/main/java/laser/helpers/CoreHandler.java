package laser.helpers;

import laser.Config;
import laser.Utils;
import laser.data.Outputs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import static laser.Utils.Assert;

public class CoreHandler {

    private static final Logger logger = LoggerFactory.getLogger(CoreHandler.class);

    private final Process coreProcess;
    private final BufferedReader errorReader;
    private final InputStream inputStream;
    private final OutputStream outputStream;
    private final int channels;
    private final int laserPin;

    private int noDataCounter;

    public CoreHandler(Config config) throws IOException, InterruptedException {
        channels = config.inputChannels.size();
        laserPin = config.laserPin;

        Assert(Files.exists(Paths.get(config.corePath)), "Can't find core at %s", config.corePath);

        String runCoreCommand = String.format(
                "sudo %s --sample_rate %s --channels %s",
                config.corePath,
                config.sampleRate * config.inputChannels.size(),
                config.inputChannels.stream().map(Object::toString).collect(Collectors.joining(" "))
        );
        logger.info("Starting core with command: '{}'", runCoreCommand);
        Runtime runtime = Runtime.getRuntime();
        coreProcess = runtime.exec(runCoreCommand.split(" "));

        runtime.addShutdownHook(new Thread(coreProcess::destroy));

        errorReader = new BufferedReader(new InputStreamReader(coreProcess.getErrorStream()));
        inputStream = coreProcess.getInputStream();
        outputStream = coreProcess.getOutputStream();

        // give core some to start and potentially fail
        Thread.sleep(5000);
        if (!coreProcess.isAlive()) {
            coreProcess.waitFor();
            BufferedReader stdReader = new BufferedReader(new InputStreamReader(inputStream));
            // read std output
            while (stdReader.ready()) {
                logger.error("core stdout: {}", stdReader.readLine());
            }
            // read error output
            while (errorReader.ready()) {
                logger.error("core errout: {}", errorReader.readLine());
            }
            throw new IllegalStateException("core process not run.");
        }
        logger.info("core running.");
    }

    public int[][] read() throws IOException {
        // read sensor data
        int available = (inputStream.available() / 2) / channels;
        if (available == 0) {
            noDataCounter++;
            if (noDataCounter > 3) {
                logger.error("No data from core.");
            }
        } else {
            noDataCounter = 0;
        }
        int[][] values = new int[channels][available];

        for (int valueIndex = 0; valueIndex < available; valueIndex++) {
            for (int channel = 0; channel < channels; channel++) {
                int b1 = inputStream.read();
                int b2 = inputStream.read();
                values[channel][valueIndex] = (b1) | (b2 << 8);
            }
        }
        logger.trace("read {} samples from each channel({})", available, channels);

        readErrorOutput();
        return values;
    }


    public void send(Outputs output, int[] outputData) throws IOException {
        // write output controlling instructions
        if (outputData != null && outputData.length > 0) {
            int extraValue = outputData.length % 2;
            outputStream.write(Utils.toLittleEndian(toGpio(output)));
            outputStream.write(Utils.toLittleEndian(outputData.length + extraValue));
            outputStream.write(Utils.toLittleEndian(outputData));
            if (extraValue > 0) {
                outputStream.write(Utils.toLittleEndian(1));
            }
        }
        outputStream.flush();

        readErrorOutput();
    }

    private void readErrorOutput() throws IOException {
        while (errorReader.ready()) {
            logger.error("core errout: {}", errorReader.readLine());
        }
    }

    // TODO: configurable
    private int toGpio(Outputs output) {
        if (output == Outputs.LASER) {
            return laserPin;
        }
        throw new IllegalStateException("Unknown output " + output.name());
    }
}
