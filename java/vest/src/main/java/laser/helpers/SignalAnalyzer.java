package laser.helpers;

import java.util.ArrayList;
import java.util.List;

public class SignalAnalyzer {

    // longest pulse can have max 4 samples, more isn't not part of valid signal
    private static final int PULSE_LENGTH_LIMIT = 4;

    private final RapidityHelper lastValues;
    private final int sensitivity;
    private final int maxSignalLength;

    private boolean receiving = false;
    private boolean signalUp = false;
    private int pulseLenghtCounter = 0;
    private int signalLenghtCounter = 0;
    private ArrayList<Integer> signal;

    public SignalAnalyzer(int sensitivity, int maxSignalLength) {
        this.sensitivity = sensitivity;
        this.maxSignalLength = maxSignalLength;
        lastValues = new RapidityHelper();
        signal = new ArrayList<>();
    }

    public List<Integer> putValue(int value) {
        List<Integer> result = null;
        // return some diff if first processed value but this should not be problem only occurring after app start
        int diff = lastValues.add(value);
        if (!signalUp && diff > sensitivity) {
            // signal changed up
            if (receiving) {
                // add pulse length only if signal not begging
                signal.add(pulseLenghtCounter);
                pulseLenghtCounter = 0;
            }
            receiving = true;
            signalUp = true;
            // need reset, instead one sample long pulses will be unrecognizable
            lastValues.reset();
        } else if (signalUp && -diff > sensitivity) {
            // signal change down
            signalUp = false;
            signal.add(pulseLenghtCounter);
            pulseLenghtCounter = 0;
            // need reset, instead one sample long pulses will be unrecognizable
            lastValues.reset();
        }
        if (receiving) {
            pulseLenghtCounter++;
            signalLenghtCounter++;
        }

        if (pulseLenghtCounter > PULSE_LENGTH_LIMIT) {
            result = signal;
            // signal end or consider as end
            resetState();
        }

        if (signalLenghtCounter > maxSignalLength) {
            // to long just return what have
            result = signal;

            resetState();
        }

        return result;
    }

    private void resetState() {
        receiving = false;
        signalUp = false;
        pulseLenghtCounter = 0;
        signalLenghtCounter = 0;
        signal = new ArrayList<>();
        lastValues.reset();
    }


    private static class RapidityHelper {

        private final int[] values;
        private int position;

        public RapidityHelper() {
            this.values = new int[2];
        }

        /**
         * @return value change between oldest value in history and new value
         */
        public int add(int value) {
            int x = values[position];
            values[position] = value;
            position = 1 - position;
            return value - x;
        }

        /**
         * remove history, left only last value
         */
        public void reset() {
            values[position] = values[1 - position];
        }
    }
}
