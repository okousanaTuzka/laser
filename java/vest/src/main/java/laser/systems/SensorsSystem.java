package laser.systems;

import laser.data.Context;
import laser.data.Event;
import laser.data.Sensors;
import laser.helpers.CoreHandler;

import java.util.Collection;

public class SensorsSystem implements AppSystem {

    private final CoreHandler coreHandler;

    public SensorsSystem(CoreHandler coreHandler) {
        this.coreHandler = coreHandler;
    }

    @Override
    public Collection<Event> update(long nowTimeMs, long deltaTimeMs, Context context) throws Exception {
        int[][] read = coreHandler.read();
        for (int i = 0; i < read.length; i++) {
            context.setSensorData(Sensors.values()[i], read[i]);
        }
        return null;
    }

    @Override
    public Collection<Event> receiveEvent(Event event, Context context) throws Exception {
        return null;
    }
}
