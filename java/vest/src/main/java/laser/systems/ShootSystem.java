package laser.systems;

import laser.data.Context;
import laser.data.Event;
import laser.data.Inputs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;

public class ShootSystem implements AppSystem {

    private static final int SHOOT_DELAY_MS = 700;

    private static final Logger logger = LoggerFactory.getLogger(ShootSystem.class);

    public ShootSystem() {
    }

    @Override
    public Collection<Event> update(long nowTimeMs, long deltaTimeMs, Context context) throws Exception {
        if (context.shootDelay != null) {
            context.shootDelay -= (int) deltaTimeMs;
            if (context.shootDelay < 0) {
                context.shootDelay = null;
            }
        }
        return null;
    }

    @Override
    public Collection<Event> receiveEvent(Event event, Context context) throws Exception {
        if (context.dead == null && context.shootDelay == null) {
            if (event instanceof Event.ButtonPressed && ((Event.ButtonPressed)event).button == Inputs.SHOOT) {
                context.shootDelay = SHOOT_DELAY_MS;
                return Collections.singleton(new Event.SendPlayerId());
            }
        }
        return null;
    }

}
