package laser.systems;

import laser.data.Context;
import laser.data.Event;
import laser.data.Inputs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.ClosedChannelException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;

public class InputSystem implements AppSystem {

    private static final Logger logger = LoggerFactory.getLogger(InputSystem.class);

    private static final Path pigpioPath = Paths.get("/dev/pigpio");
    private static final Path pigoutPath = Paths.get("/dev/pigout");
    private static final int BUTTON_PIN = 6;
    private OutputStream outputStream;
    private BufferedReader inputStream;
    private final byte[] readCmd;

    private boolean pressed = false;

    public InputSystem() throws IOException {
        if (!Files.exists(pigpioPath)) {
            throw new IllegalStateException(pigpioPath.toString() + " not exits.");
        }
        if (!Files.exists(pigoutPath)) {
            throw new IllegalStateException(pigoutPath.toString() + " not exits.");
        }
        outputStream = Files.newOutputStream(pigpioPath);
        openInputStream();

        outputStream.write(String.format("modes %s r\n", BUTTON_PIN).getBytes(StandardCharsets.US_ASCII));
        outputStream.flush();
        readCmd = (String.format("r %s\n", BUTTON_PIN).getBytes(StandardCharsets.US_ASCII));
    }

    @Override
    public Collection<Event> update(long nowTimeMs, long deltaTimeMs, Context context) throws Exception {
        try {
            outputStream.write(readCmd);
            outputStream.flush();
        } catch (ClosedChannelException e) {
            openOutputStream();
            throw e;
        }
        String line;
        try {
            line = inputStream.readLine();
        } catch (ClosedChannelException e) {
            openInputStream();
            throw e;
        }
        if ("1".equals(line)) {
            if (!pressed) {
                logger.info("Shoot button pressed");
                pressed = true;
                return Collections.singleton(new Event.ButtonPressed(Inputs.SHOOT));
            }
        } else if ("0".equals(line)) {
            pressed = false;
        } else if (line == null) {
            logger.error("Stream end");
        } else {
            logger.error("Unexpected value {}", line);
        }
        return null;
    }

    @Override
    public Collection<Event> receiveEvent(Event event, Context context) throws Exception {
        return null;
    }

    private void openInputStream() throws IOException {
        inputStream = Files.newBufferedReader(pigoutPath);
    }

    private void openOutputStream() throws IOException {
        outputStream = Files.newOutputStream(pigpioPath);
    }
}
