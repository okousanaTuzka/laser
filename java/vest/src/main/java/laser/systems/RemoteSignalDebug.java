package laser.systems;

import laser.Config;
import laser.Utils;
import laser.data.Context;
import laser.data.Event;
import laser.data.Sensors;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Collection;

public class RemoteSignalDebug implements AppSystem {

    private final ByteBuffer buffer;
    private final Config config;
    private SocketChannel socketChannel;


    public RemoteSignalDebug(Config config) throws IOException {
        this.config = config;

        buffer = ByteBuffer.allocate(1024 * 1024);
    }

    @Override
    public Collection<Event> update(long nowTimeMs, long deltaTimeMs, Context context) throws Exception {
        // hack

        if (socketChannel == null && config.remoteSignalDebugServer != null) {
            String[] split = config.remoteSignalDebugServer.split(":");
            socketChannel = SocketChannel.open(new InetSocketAddress(split[0], Integer.parseInt(split[1])));
            socketChannel.configureBlocking(false);
        }

        try {
            int[][] data = new int[2][];
            data[0] = context.getSensorData(Sensors.SENZOR_1);
            data[1] = context.getSensorData(Sensors.SENZOR_2);

            buffer.clear();
            for (int i = 0; i < data[0].length; i++) {
                for (int j = 0; j < 2; j++) {
                    byte[] bytes = Utils.toLittleEndian(data[j][i]);
                    buffer.put(bytes[0]);
                    buffer.put(bytes[1]);
                }
            }
            buffer.flip();
            socketChannel.write(buffer);

            buffer.clear();
            socketChannel.read(buffer);
        } catch (IOException e) {
            socketChannel = null;
            throw e;
        }

        return null;
    }

    @Override
    public Collection<Event> receiveEvent(Event event, Context context) throws Exception {
        return null;
    }
}
