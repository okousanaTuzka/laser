package laser.systems;

import laser.data.Context;
import laser.data.Event;

import java.util.Collection;

public interface AppSystem {

    /**
     * @return events or null
     */
    Collection<Event> update(long nowTimeMs, long deltaTimeMs, Context context) throws Exception;

    /**
     * should only react to event
     * @return events or null
     */
    Collection<Event> receiveEvent(Event event, Context context) throws Exception;
}
