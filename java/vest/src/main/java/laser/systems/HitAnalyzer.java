package laser.systems;

import laser.Config;
import laser.Utils;
import laser.data.Context;
import laser.data.Event;
import laser.data.Sensors;
import laser.helpers.SignalAnalyzer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class HitAnalyzer implements AppSystem {

    private static final Logger logger = LoggerFactory.getLogger(AppSystem.class);

    private final SignalAnalyzer[] analyzers = new SignalAnalyzer[Sensors.values().length];
    private final int bitsInSignal;

    public HitAnalyzer(Config config) {
        this.bitsInSignal = config.encodedBits;
        for (int i = 0; i < analyzers.length; i++) {
            // TODO: this limits need some tweak, auto calculate or config
            analyzers[i] = new SignalAnalyzer(200, 200);
        }
    }

    @Override
    public Collection<Event> update(long nowTimeMs, long deltaTimeMs, Context context) throws Exception {

        ArrayList<Event> events = new ArrayList<>();

        for (Sensors input : Sensors.values()) {
            int[] inputData = context.getSensorData(input);
            if (inputData != null && inputData.length > 0) {
                for (int i = 0; i < inputData.length; i++) {
                    List<Integer> received = analyzers[input.ordinal()].putValue(inputData[i]);
                    if (received != null) {
                        Integer decoded = Utils.decode(received, bitsInSignal);
                        if (decoded != null) {
                            logger.info("receive hit by {}", decoded);
                            events.add(new Event.Hitted(decoded));
                        }
                    }
                }
            }
        }

        return events;
    }

    @Override
    public Collection<Event> receiveEvent(Event event, Context context) throws Exception {
        return null;
    }

}
