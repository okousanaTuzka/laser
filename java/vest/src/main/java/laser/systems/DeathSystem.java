package laser.systems;

import laser.data.Context;
import laser.data.Event;
import laser.data.components.Dead;

import java.util.Collection;

public class DeathSystem implements AppSystem {

    private static final int DEAD_DURATION_MS = 5000;

    @Override
    public Collection<Event> update(long nowTimeMs, long deltaTimeMs, Context context) throws Exception {
        if (context.dead != null) {
            context.dead.remaining -= deltaTimeMs;
            if (context.dead.remaining <= 0) {
                context.dead = null;
            }
        }
        return null;
    }

    @Override
    public Collection<Event> receiveEvent(Event event, Context context) throws Exception {
        if (event instanceof Event.Hitted) {
            context.dead = new Dead(DEAD_DURATION_MS);
        }
        return null;
    }
}
