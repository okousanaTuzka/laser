package laser.systems;

import laser.Config;
import laser.Utils;
import laser.data.Context;
import laser.data.Event;
import laser.data.Outputs;
import laser.helpers.CoreHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;

public class OutputSystem implements AppSystem {

    private static final Logger logger = LoggerFactory.getLogger(OutputSystem.class);

    private final CoreHandler coreHandler;
    private final int[] playerIdSignal;

    public OutputSystem(CoreHandler coreHandler, Config config) {
        this.coreHandler = coreHandler;

        this.playerIdSignal = Utils.encode(config.playerId, config.encodedBits, Duration.ofMillis(config.shootLengthMs), config.sampleRate);
        logger.info(Arrays.toString(playerIdSignal));
    }

    @Override
    public Collection<Event> update(long nowTimeMs, long deltaTimeMs, Context context) throws Exception {
        return null;
    }

    @Override
    public Collection<Event> receiveEvent(Event event, Context context) throws Exception {
        if (event instanceof Event.SendPlayerId) {
            coreHandler.send(Outputs.LASER, playerIdSignal);
        } else if (event instanceof Event.SendCustomSignal) {
            // for testing purpose
            coreHandler.send(Outputs.LASER, ((Event.SendCustomSignal) event).signal);
        }
        return null;
    }
}
