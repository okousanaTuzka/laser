package laser.systems;

import laser.Config;
import laser.Utils;
import laser.data.Context;
import laser.data.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;

public class SomeTestingSystem implements AppSystem {

    private static final Logger logger = LoggerFactory.getLogger(ShootSystem.class);

    private final Random random = new Random();
    private final int[][] signals;

    private int timer;
    private int shootedNumber = -1;

    public SomeTestingSystem(Config config) {
        signals = new int[(int) Math.pow(2, config.encodedBits)][];
        for (int i = 0; i < signals.length; i++) {
            signals[i] = Utils.encode(i, config.encodedBits, Duration.ofMillis(10), config.sampleRate);
        }
    }

    @Override
    public Collection<Event> update(long nowTimeMs, long deltaTimeMs, Context context) throws Exception {
        timer += deltaTimeMs;
        if (timer > 2_000) {
            if (shootedNumber != -1) {
                logger.error("--------- Shooted number isn't detected");
            }
            timer = 0;
            shootedNumber = random.nextInt(signals.length);
            return Collections.singleton(new Event.SendCustomSignal(signals[shootedNumber]));
        }
        return null;
    }

    @Override
    public Collection<Event> receiveEvent(Event event, Context context) throws Exception {
        if (event instanceof Event.Hitted) {
            if (((Event.Hitted) event).shooter == shootedNumber) {
                logger.info(" --------- Shoot {} successfully detected", shootedNumber);
            } else {
                logger.error("--------- Detected shootNumber:{} but expected {}", ((Event.Hitted) event).shooter, shootedNumber);
            }
            shootedNumber = -1;
        }
        return null;
    }
}
