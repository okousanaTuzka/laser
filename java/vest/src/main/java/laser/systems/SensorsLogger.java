package laser.systems;

import laser.Config;
import laser.data.Context;
import laser.data.Event;
import laser.data.Sensors;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class SensorsLogger implements AppSystem {

    private static final int SURROUNDING = 100;
    private static final int SENSITIVITY = 100;

    private final List<Signal> signals = new ArrayList<>(Sensors.values().length);
    private final Config config;

    public SensorsLogger(Config config) throws IOException {
        this.config = config;
        if (config.sensorsLog != null) {
            for (int i = 0; i < Sensors.values().length; i++) {
                signals.add(new Signal());
            }
        }
    }

    @Override
    public Collection<Event> update(long nowTimeMs, long deltaTimeMs, Context context) throws Exception {
        if (config.sensorsLog != null) {
            for (Sensors sensor : Sensors.values()) {
                for (int value : context.getSensorData(sensor)) {
                    Integer[] data = signals.get(sensor.ordinal()).add(value);
                    if (data != null) {
                        Files.write(config.sensorsLog, (Arrays.toString(data) + "\n").getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                    }
                }
            }
        }
        return null;
    }

    @Override
    public Collection<Event> receiveEvent(Event event, Context context) throws Exception {
        return null;
    }

    private static class Signal {
        private final ArrayDeque<Integer> signal;
        private final Filter filter;
        private int sampleCounter;
        private boolean logging;

        private Signal() {
            signal = new ArrayDeque<>();
            filter = new Filter(2);
        }

        public Integer[] add(int value) {
            signal.add(value);

            if (filter.add(value) > SENSITIVITY) {
                logging = true;
                sampleCounter = 0;
            }
            if (logging) {
                sampleCounter++;
            }
            if (!logging && signal.size() > SURROUNDING) {
                signal.remove();
            }
            if (logging && sampleCounter > SURROUNDING) {
                logging = false;
                Integer[] data = signal.toArray(new Integer[0]);
                signal.clear();
                return data;
            }
            return null;
        }
    }

    private static class Filter {

        private final int[] values;
        private int position;

        public Filter(int size) {
            this.values = new int[size + 1];
        }

        /**
         * @return value change between oldest value in history and new value
         */
        public int add(int value) {
            values[position++] = value;
            if (position == values.length) position = 0;
            return value - values[position];
        }

    }
}
