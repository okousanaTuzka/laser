package laser.systems;

import laser.data.Context;
import laser.data.Event;
import laser.data.components.Score;

import java.util.Collection;

public class ScoreSystem implements AppSystem {

    private static final int HIT_SCORE_POINTS = 10;

    @Override
    public Collection<Event> update(long nowTimeMs, long deltaTimeMs, Context context) throws Exception {
        return null;
    }

    @Override
    public Collection<Event> receiveEvent(Event event, Context context) throws Exception {
        if (event instanceof Event.Hitted) {
            if (context.score == null) {
                context.score = new Score();
            }
            if (context.score.scoreMap.containsKey(((Event.Hitted) event).shooter)) {
                context.score.scoreMap.put(((Event.Hitted) event).shooter, context.score.scoreMap.get(((Event.Hitted) event).shooter) + HIT_SCORE_POINTS);
            }
            context.score.scoreMap.put(((Event.Hitted) event).shooter, HIT_SCORE_POINTS);
        }
        return null;
    }
}
