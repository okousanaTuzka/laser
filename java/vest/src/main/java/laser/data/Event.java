package laser.data;


public abstract class Event {

    public static class SendCustomSignal extends Event {
        public final int[] signal;
        public SendCustomSignal(int[] signal) {
            this.signal = signal;
        }
    }

    public static class SendPlayerId extends Event {}

    public static class Shooting extends Event {}

    public static class Hitted extends Event {
        public final int shooter;
        public Hitted(int shooter) {
            this.shooter = shooter;
        }
    }

    public static class ButtonPressed extends Event {

        public final Inputs button;

        public ButtonPressed(Inputs button) {
            this.button = button;
        }
    }

}
