package laser.data;


import laser.data.components.Dead;
import laser.data.components.Score;

public class Context {

    // TODO: this maybe need rework
    private final int[][] sensorsData = new int[Sensors.values().length][];

    // not ideal but sufficient for now
    // "components":
    public Dead dead;
    public Score score;
    public Integer shootDelay;


    /**
     * @return data or null
     */
    public int[] getSensorData(Sensors input) {
        return sensorsData[input.ordinal()];
    }

    public void setSensorData(Sensors input, int[] data) {
        sensorsData[input.ordinal()] = data;
    }

}
