package laser;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import java.nio.file.Path;
import java.util.List;

public class Config {

    @NotEmpty
    public String corePath;

    // list of channels which will be read, match core channel specification
    @NotEmpty
    public List<Integer> inputChannels;

    // sample rate for each channel (differ from core. core use sample rate for all channels in sum)
    @Positive
    public int sampleRate;

    // this will be encoded and "shooted"
    @Positive
    public int playerId;

    // number of bits used for "shooted" number
    @Positive
    public int encodedBits;

    // how long should be single shoot ("shooted" number is repeated)
    @Positive
    public int shootLengthMs;

    // pin where is connected device for shooting
    @Min(4)
    @Max(5)
    public int laserPin = 5;

    // ip where is running "visualizer"
    public String remoteSignalDebugServer;

    // to this file will be saved same parts of recorded signals from sensors
    public Path sensorsLog;

}
