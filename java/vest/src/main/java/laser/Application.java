package laser;

import laser.data.Context;
import laser.data.Event;
import laser.systems.AppSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Application {

    private static final int TARGET_DELTA_TIME = 20;
    private static final int SYSTEM_UPDATE_TIMEOUT_MS = 5;

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    private final ExecutorService executorService = Executors.newFixedThreadPool(1);
    private final AppSystem[] systems;

    private boolean run;

    public Application(AppSystem ... systems) {
        this.systems = systems;
        this.run = true;
    }

    public void run() {

        logger.info("start update loop.");

        Context context = new Context();
        long lastStart = System.currentTimeMillis();

        while (run) {
            long start = System.currentTimeMillis();
            long deltaTime = start - lastStart;
            lastStart = start;

            // cleanup input|output data for safe, IO data handling maybe need rework but i didn't decide yet how implement it.
//            context.setInputData(Senzors.SENZOR_1, null);
//            context.setInputData(Senzors.SENZOR_2, null);
//            context.setOutputData(Outputs.LASER, null);

            for (AppSystem system : systems) {
                Future<Collection<Event>> future = executorService.submit(() -> system.update(start, deltaTime, context));
                try {
                    handleEvents(future.get(SYSTEM_UPDATE_TIMEOUT_MS, TimeUnit.MILLISECONDS), context);
                } catch (InterruptedException e) {
                    run = false;
                } catch (ExecutionException e) {
                    logger.error(String.format("%s.update() call fail", system.getClass().getSimpleName()), e);
                } catch (TimeoutException e) {
                    // TODO: need force interrupt
                    logger.error(String.format(
                            "%s.update() don't respond in %s ms",
                            system.getClass().getSimpleName(),
                            SYSTEM_UPDATE_TIMEOUT_MS));
                    future.cancel(true);
                }
            }
            long updateTime = System.currentTimeMillis() - start;
            if (updateTime < TARGET_DELTA_TIME) {
                try {
                    Thread.sleep(TARGET_DELTA_TIME - updateTime);
                } catch (InterruptedException e) {
                    run = false;
                }
            } else {
                logger.warn(String.format("Update loop take %s ms, TARGET_DELTA_TIME is %s ms", updateTime, TARGET_DELTA_TIME));
            }
            logger.trace("Update loop take {} ms", updateTime);
        }

    }

    private void handleEvents(Collection<Event> events, Context context) {
        if (events == null) return;

        ArrayList<Event> newEvents;

        do {
            newEvents = new ArrayList<>();
            for (Event event : events) {
                logger.trace("processing event {}", event.getClass().getSimpleName());
                for (AppSystem system : systems) {
                    try {
                        Collection<Event> e = system.receiveEvent(event, context);
                        if (e != null) newEvents.addAll(e);
                    } catch (Exception e) {
                        logger.error(String.format("System %s fail while handling event %s", system.getClass().getSimpleName(), event.getClass().getSimpleName()), e);
                    }
                }
            }
            events = newEvents;
        } while (!newEvents.isEmpty());

    }

}
