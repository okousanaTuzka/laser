package laser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import laser.helpers.CoreHandler;
import laser.systems.DeathSystem;
import laser.systems.HitAnalyzer;
import laser.systems.InputSystem;
import laser.systems.OutputSystem;
import laser.systems.RemoteSignalDebug;
import laser.systems.ScoreSystem;
import laser.systems.SensorsLogger;
import laser.systems.SensorsSystem;
import laser.systems.ShootSystem;
import laser.systems.SomeTestingSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    private static final Path CONFIG_PATH = Paths.get("config.yml");


    private static final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
    private static final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();


    public static void main(String[] args) throws IOException, URISyntaxException, InterruptedException {

        Path currentPath = Paths.get(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParent();
        Config config = loadConfig(currentPath.resolve(CONFIG_PATH));

        logger.info("Initializing application.");

        CoreHandler coreHandler = new CoreHandler(config);

        Application application = new Application(
                new DeathSystem(),
                new SensorsSystem(coreHandler),
                new InputSystem(),
                new OutputSystem(coreHandler, config),
                new HitAnalyzer(config),
                new ShootSystem(),
                new ScoreSystem(),
                new SensorsLogger(config),
//                new SomeTestingSystem(config)
                new RemoteSignalDebug(config)
        );

        application.run();
    }


    public static Config loadConfig(Path path) throws IOException {

        Config config = objectMapper.readValue(path.toFile(), Config.class);

        Set<ConstraintViolation<Config>> validate = validator.validate(config);
        if (!validate.isEmpty()) {
            String errorMsg = "Wrong configuration in file" + path.toAbsolutePath() + ": ";
            for (ConstraintViolation<Config> v : validate) {
                errorMsg += v.getPropertyPath() + " " + v.getMessage() + ". ";
            }
            throw new IOException(errorMsg);
        }

        return config;
    }
}
