package laser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.WritableByteChannel;
import java.util.concurrent.TimeUnit;

public class Client implements Runnable {


    private final SocketChannelHandler socketChannel;
    private String cmd;

    public Client(String hostName, int port, String cmd) {
        socketChannel = new SocketChannelHandler(() -> {
            SocketChannel ss = SocketChannel.open(new InetSocketAddress(hostName, port));
            ss.configureBlocking(false);
            return ss;
        });
        this.cmd = cmd;
        Runtime.getRuntime().addShutdownHook(new Thread(() ->
                Utils.tryAndLogErr(socketChannel::close, "close socketChannel")));
    }

    @Override
    public void run() {
        Process process = null;
        try {
            Runtime runtime = Runtime.getRuntime();
            ByteBuffer inBuffer = ByteBuffer.allocateDirect(128 * 1024);
            ByteBuffer outBuffer = ByteBuffer.allocateDirect(128 * 1024);

            process = runtime.exec(cmd.split(" "));
            BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            ReadableByteChannel inputStream = Channels.newChannel(process.getInputStream());
            WritableByteChannel outputStream = Channels.newChannel(process.getOutputStream());

            while (!Thread.interrupted()) {
                boolean readed = false;
                if (process.getInputStream().available() > 1) {
                    readed = true;
                    inputStream.read(inBuffer);
                    inBuffer.flip();
                    socketChannel.write(inBuffer);
                    inBuffer.compact();
                }
                if (socketChannel.read(outBuffer) > 0) {
                    readed = true;
                    socketChannel.read(outBuffer);
                    outBuffer.flip();
                    outputStream.write(outBuffer);
                    process.getOutputStream().flush();
                    outBuffer.compact();
                }
                if (errorReader.ready()) {
                    System.err.println(errorReader.readLine());
                    readed = true;
                }
                if (!readed) {
                    Thread.sleep(20);
                }
            }

        } catch (InterruptedException ignore) {
        } catch (IOException e) {
            throw new IllegalStateException(e);
        } finally {
            Utils.tryAndLogErr(socketChannel::close, "close dataSender");
            if (process != null) {
                process.destroy();
                Process effectivelyFinalVariable = process;
                if (!Utils.tryAndLogErr(() -> effectivelyFinalVariable.waitFor(30, TimeUnit.SECONDS), "close subprocess")) {
                    process.destroyForcibly();
                }
            }
        }
        System.out.println("client stopped");
    }
}
