package laser;

public class Utils {

    public interface Action {
        void call() throws Exception;
    }

    /**
     * execute code and if fail print exception and massage
     * @return true if code is successfully executed, false in case of any exception
     */
    public static boolean tryAndLogErr(Utils.Action fnc, String message) {
        try {
            fnc.call();
            return true;
        } catch (Exception e) {
            System.out.println("FAIL: " + message);
            e.printStackTrace();
            return false;
        }
    }
}
