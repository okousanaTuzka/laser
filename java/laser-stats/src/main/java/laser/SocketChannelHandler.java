package laser;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.concurrent.ConcurrentLinkedQueue;

public class SocketChannelHandler {

    private SocketChannel channel;
    private SocketChannelFactory factory;

    public SocketChannelHandler(SocketChannelFactory factory) {
        this.factory = factory;
    }

    public void write(ByteBuffer buffer) throws InterruptedException {
        ensureOpen();
        if (buffer.remaining() > 0) {
            try {
                channel.write(buffer);
            } catch (IOException e) {
                e.printStackTrace();
                buffer.position(buffer.limit());
                channel = null;
            }
        }
    }

    public int read(ByteBuffer buffer) throws InterruptedException {
        ensureOpen();
        if (buffer.remaining() > 0) {
            try {
                int read = channel.read(buffer);
                if (read == -1) {
                    channel = null;
                }
                return read;
            } catch (IOException e) {
                e.printStackTrace();
                buffer.position(buffer.limit());
                channel = null;
            }
        }
        return 0;
    }

    public void ensureOpen() throws InterruptedException {
        while (channel == null || !channel.isOpen()) {
            try {
                channel = factory.openSocketChannel();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Thread.sleep(1000);
        }
    }

    public void close() throws IOException {
        if (channel != null) channel.close();
    }

    public interface SocketChannelFactory {
        SocketChannel openSocketChannel() throws IOException;
    }
}