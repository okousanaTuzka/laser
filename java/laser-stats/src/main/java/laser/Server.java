package laser;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;


public class Server {

    private static ServerSocketChannel serverChannel;
    private final SocketChannelHandler socketChannelHandler;

    public Server(int port) throws IOException {
        serverChannel = ServerSocketChannel.open();
        serverChannel.bind(new InetSocketAddress(port));

        socketChannelHandler = new SocketChannelHandler(() -> {
            SocketChannel ss = serverChannel.accept();
            ss.configureBlocking(false);
            return ss;
        });


        Runtime.getRuntime().addShutdownHook(new Thread(this::close));
    }

    public SocketChannelHandler getSocketChannel() {
        return socketChannelHandler;
    }

    public void close() {
        Utils.tryAndLogErr(socketChannelHandler::close, "close client");
        Utils.tryAndLogErr(serverChannel::close, "close server");
    }

}
