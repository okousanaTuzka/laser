package laser;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ConcurrentLinkedQueue;

import laser.visualization.Gui;

public class Main {

    public static int PORT = 12345;

    public static void main(String[] args) throws IOException, InterruptedException {
        if (args.length > 0) {
            switch (args[0]) {
                case "server":
                    Path file = null;
                    int channels = 1;
                    for (int i = 1; i < args.length - 1; i++) {
                        if (args[i].equals("--file")) {
                            file = Paths.get(args[++i]);
                        } else if (args[i].equals("--channels")) {
                            channels = Integer.parseInt(args[++i]);
                        }
                    }

                    Server server = new Server(PORT);
                    SocketChannelHandler socketChannel = server.getSocketChannel();

                    ConcurrentLinkedQueue<Integer> inQueue = new ConcurrentLinkedQueue();
                    ConcurrentLinkedQueue<Integer> outQueue = new ConcurrentLinkedQueue();

                    ByteBuffer sendBuffer = ByteBuffer.allocateDirect(128 * 1024);
                    ByteBuffer readBuffer = ByteBuffer.allocateDirect(128 * 1024);


                    Thread thread = new Thread(() -> {
                            while (!Thread.interrupted()) {
                                try {
                                    while (!outQueue.isEmpty() && sendBuffer.remaining() > 4) {
                                        int x = outQueue.remove();
                                        sendBuffer.put((byte)x);
                                        sendBuffer.put((byte)(x >> 8));
                                        sendBuffer.put((byte)(x >> 16));
                                        sendBuffer.put((byte)(x >> 24));
                                    }
                                    sendBuffer.flip();
                                    socketChannel.write(sendBuffer);
                                    sendBuffer.compact();


                                    socketChannel.read(readBuffer);
                                    readBuffer.flip();
                                    while (readBuffer.remaining() >= 2) {
                                        int b1 = (readBuffer.get()) & 0xff | (readBuffer.get()) << 8;
                                        inQueue.add(b1);
                                    }
                                    readBuffer.compact();

                                    Thread.sleep(20);

                                } catch (InterruptedException e) {
                                    Thread.currentThread().interrupt();
                                }
                            }});
                    thread.setDaemon(true);
                    thread.start();

                    Gui.startSync(inQueue, outQueue, channels, file);

                    break;
                case "client":
                    String hostName = null;
                    String cmd = null;
                    for (int i = 1; i < args.length; i++) {
                        if (args[i].equals("--server")) {
                            hostName = args[++i];
                        } else if (i == args.length - 1){
                            cmd = args[i];
                        }
                    }
                    if (hostName == null) {
                        System.out.println("require --server <hostname|ip>");
                        System.exit(1);
                    }
                    if (cmd == null) {
                        System.out.println("require command in string for launch subprocess as last argument: 'run-something --arg 1 --arg 2'");
                        System.exit(1);
                    }
                    new Client(hostName, PORT, cmd).run();

                    break;
                case "--help":
                case "-h":
                    printHelp();
                    break;
                default:
            }
        }
        if (args.length == 0) {
            printHelp();
        }
    }

    public static void printHelp() {
        System.out.println("required <server|client> = client run at shootpi and pipe-in output from 'core', server run at pc and see charts");
        System.out.println("\tserver:");
        System.out.println("\t\toptional: --file <path>          = save data from sample charts to file");
        System.out.println("\t\toptional: --channels             = number of channels");
        System.out.println("\tclient:");
        System.out.println("\t\trequired: --server <hostname|ip> = where running server");
        System.out.println("\t\trequired: '<command>'            = command for start core");
    }
}
