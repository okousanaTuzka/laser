package laser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

import laser.visualization.Gui;

/**
 *
 * @author David
 */
public class Test {

    private static final int CHANNELS = 2;

    public static void main(String[] args) throws IOException, InterruptedException {
        ConcurrentLinkedQueue<Integer> inQueue = new ConcurrentLinkedQueue();
        ConcurrentLinkedQueue<Integer> outQueue = new ConcurrentLinkedQueue();

        ArrayList<SignalSource> channelsSource = new ArrayList<>();
        for (int channel = 0; channel < CHANNELS; channel++) {
            channelsSource.add(new SignalSource());
        }

        Thread thread = new Thread(() -> {
            while (true) {

                channelsSource.forEach(source -> inQueue.add(source.get()));
                outQueue.clear();

                try {
                    Thread.sleep(50);
                } catch (InterruptedException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });
        thread.setDaemon(true);
        thread.start();

        Gui.startSync(inQueue, outQueue, CHANNELS, null);
    }

    private static class SignalSource {

        private Random random = new Random();
        private int level = random.nextInt(100);
        private int hightLevel = 1000;
        private Signaling signaling = new Signaling();

        public int get() {

            if (!signaling.hasNext() && random.nextDouble() < 0.0005) {
                signaling.reset();
            }
            if (signaling.nextReceiving()) {
                hightLevel += (int) ((random.nextFloat() - 0.5) * 2.1);
                hightLevel = Math.max(800, Math.min(4096, hightLevel));
                return (int) (hightLevel + random.nextGaussian() * 2);
            } else {
                level += (int) ((random.nextFloat() - 0.5) * 2.1);
                level = Math.min(200, Math.abs(level));
                return (int) (level + random.nextGaussian() * 2);
            }
        }

    }

    private static class Signaling {

        private Random random = new Random();

        private boolean up;
        private int coutDown;
        private int lenght;

        void reset() {
            up = true;
            coutDown = 0;
            lenght = 3 + random.nextInt(100);
        }

        boolean hasNext() {
            return lenght > 0;
        }

        boolean nextReceiving() {
            if (coutDown == 0) {
                coutDown = 2 + random.nextInt(10);
                up = !up;
                lenght--;
            }
            coutDown--;
            return lenght > 0 && up;
        }
    }
}
