package laser.visualization;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.ValueAxis;
import javafx.scene.chart.XYChart;

public class Chart extends ScatterChart<Number, Number> {

    private static final String[] COLORS = {"red", "green", "darkorange", "purple", "darkblue", "darkcyan", "Sienna", "#646464" };

    private static final int DATA_OFFSET = 2;
    private static final int VERTICAL_LINE_DATA = 0;

    private List<Integer> positions = new ArrayList<>();

    public Chart() {
        super(new NumberAxis(), new NumberAxis());
        setAnimated(false);
        setLegendVisible(false);
//        setPrefSize(1600, 600);
        setPrefHeight(200);
        ((NumberAxis) getXAxis()).autoRangingProperty().set(false);
        ((NumberAxis) getXAxis()).minorTickVisibleProperty().set(false);
        ((NumberAxis) getXAxis()).setMinorTickVisible(false);
        ((NumberAxis) getXAxis()).setTickUnit(5000);

        ((NumberAxis) getYAxis()).setForceZeroInRange(true);

        // vertical line
        XYChart.Series<Number, Number> series = new XYChart.Series<>();
        series.getData().add(new Data<>(0, 0));
        getData().add(series);
        Set<Node> lookupAll = lookupAll(".chart-symbol.series0");
        for (Node node : lookupAll) {
            node.setStyle(""
                  + "-fx-background-radius: 1px;"
                  + "-fx-background-color: gray;"
                  + "-fx-padding: 10000px 1px 1px 1px;"
                  + "-fx-snap-to-pixel: true;"
            );
        }

        // hack: limit min axis value
        XYChart.Series<Number, Number> series2 = new XYChart.Series<>();
        series2.getData().add(new Data<>(0, 200));
        getData().add(series2);
        Set<Node> lookupAll2 = lookupAll(".chart-symbol.series1");
        for (Node node : lookupAll2) {
            node.setStyle(""
                  + "-fx-background-radius: 0px;"
                  + "-fx-padding: 0px 0px 0px 0px;"
            );
        }

        // need one serie because then is not necessary handle case without data
        newDataSerie();

        getXAxis().widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                resized();
            }
        });
    }

    public void putValue(int value, int channel) {
        if (getData().size() <= DATA_OFFSET + channel) {
            newDataSerie();
        }
        ObservableList<Data<Number, Number>> data = getData().get(DATA_OFFSET + channel).getData();
        if (data.isEmpty()) return;
        data.get(getPosition(channel)).setYValue(value);
        increasePosition(channel);
    }

    private int getPosition(int channel) {
        if (positions.get(channel) >= getData().get(DATA_OFFSET + channel).getData().size()) {
            positions.set(channel, 0);
        }
        return positions.get(channel);
    }

    private void increasePosition(int channel){
        positions.set(channel, positions.get(channel) + 1);
        // move vertical line
        ObservableList<Data<Number, Number>> data = getData().get(VERTICAL_LINE_DATA).getData();
        data.get(VERTICAL_LINE_DATA).setXValue(positions.get(channel));
    }

    private void resized(){
        int width = (int) (getXAxis().getWidth());
        for (int i = 0; i < positions.size(); i++) {
            if (positions.get(i) >= width) {
                positions.set(i, 0);
            }
        }
        ((ValueAxis)getXAxis()).setUpperBound(width);
        for (int dataSerie = DATA_OFFSET; dataSerie < getData().size(); dataSerie++) {
            ObservableList<Data<Number, Number>> data = getData().get(dataSerie).getData();
            if (width == data.size()) continue;
            if (width > data.size()) {
                for (int j = data.size(); j < width; j++) {
                    data.add(new Data<>(j, 0));
                }
            } else {
                for (int j = data.size() - 1; j >= width; j--) {
                    data.remove(j);
                }
            }
            Set<Node> lookupAll = lookupAll(".chart-symbol.series" + dataSerie);
            for (Node node : lookupAll) {
                node.setStyle(""
                        + "-fx-stroke-width: 1px;"
                        + "-fx-background-radius: 1px;"
                        + "-fx-background-color: " + getColor(dataSerie - DATA_OFFSET) + ";"
                        + "-fx-padding: 1px 1px 1px 1px;"
                        + "-fx-snap-to-pixel: true;"
                );
            }
        }
    }

    private void newDataSerie() {
        getData().add(new XYChart.Series<>());

        int serieNumber = getData().size() - 1;

        Set<Node> lookupAll = lookupAll(".chart-symbol.series" + serieNumber);
        for (Node node : lookupAll) {
            node.setStyle(""
                    + "-fx-stroke-width: 1px;"
                    + "-fx-background-radius: 1px;"
                    + "-fx-background-color: " + getColor(serieNumber - DATA_OFFSET) + ";"
                    + "-fx-padding: 1px 1px 1px 1px;"
                    + "-fx-snap-to-pixel: true;"
            );
        }
        positions.add(0);

        resized();
    }

    private String getColor(int i) {
        return COLORS[i % COLORS.length];
    }
}
