package laser.visualization;

import java.util.List;
import java.util.Set;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.ValueAxis;
import javafx.scene.chart.XYChart;

public class SmallChart extends ScatterChart<Number, Number> {

    public SmallChart() {
        super(new NumberAxis(), new NumberAxis());
        setAnimated(false);
        setLegendVisible(false);
        setPrefHeight(200);
//        setPrefSize(1600, 600);
        ((NumberAxis) getXAxis()).autoRangingProperty().set(false);
        ((NumberAxis) getXAxis()).minorTickVisibleProperty().set(false);
        ((NumberAxis) getXAxis()).setMinorTickVisible(false);
        ((NumberAxis) getXAxis()).setTickUnit(5000);

        ((NumberAxis) getYAxis()).setForceZeroInRange(true);

    }

    public void putValues(List<Integer> values) {
        XYChart.Series<Number, Number> series = new XYChart.Series<>();
        for (int i = 0; i < values.size(); i++) {
            series.getData().add(new Data<>(i, values.get(i)));
        }
        getData().add(series);
        ((ValueAxis) getXAxis()).setUpperBound(values.size() - 1);
        Set<Node> lookupAll = lookupAll(".chart-symbol.series0");
        for (Node node : lookupAll) {
            node.setStyle(""
                + "-fx-stroke-width: 1px;"
                + "-fx-background-radius: 1px;"
                + "-fx-background-color: black;"
                + "-fx-padding: 1px 1px 1px 1px;"
                + "-fx-snap-to-pixel: true;"
            );
        }

        double width = getYAxis().getPrefWidth() + values.size() + 50;
//        setMinWidth(width);
        setPrefWidth(width);
        setMaxWidth(width);
    }

    public void setZoom(double zoom) {
        double width = getYAxis().getPrefWidth() + getData().get(0).getData().size() + 50;
        width *= zoom;
        setPrefWidth(width);
        setMaxWidth(width);
    }

}
