package laser.visualization;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;

public class Gui extends Application {

    // HACK
    private static Path file;
    private static int channels = 1;
    private static Queue<Integer> inQueue;
    private static Queue<Integer> outQueue;


    private Chart chart;
    private List<Analyzer> analyzers;
    private Pane chartStack;
    private Scene scene;
    private int currentChannel = 0;

    private double zoom = 1;

    @Override
    public void start(Stage primaryStage) throws Exception {
        System.out.println(String.format("init with %d channels", channels));

        analyzers = new ArrayList<>(channels);
        for (int i = 0; i < channels; i++) {
            analyzers.add(new Analyzer());
        }

        TextField sensitivity = new TextField();
        sensitivity.setPrefWidth(60);
        sensitivity.setTextFormatter(new TextFormatter<>(new NumberStringConverter()));
        sensitivity.setText(analyzers.get(0).getSensivity() + "");
        sensitivity.textProperty().addListener((o) -> analyzers.forEach(a -> a.setSensivity(Integer.parseInt(sensitivity.getText()))));

        TextField rapidity = new TextField();
        rapidity.setPrefWidth(30);
        rapidity.setTextFormatter(new TextFormatter<>(new NumberStringConverter()));
        rapidity.setText(analyzers.get(0).getRapidity() + "");
        rapidity.textProperty().addListener((o) -> analyzers.forEach(a -> a.setRapidity(Integer.parseInt(rapidity.getText()))));

        TextField surroundings = new TextField();
        surroundings.setPrefWidth(60);
        surroundings.setTextFormatter(new TextFormatter<>(new NumberStringConverter()));
        surroundings.setText(analyzers.get(0).getSurroundings() + "");
        surroundings.textProperty().addListener((o) -> analyzers.forEach(a -> a.setSurroundings(Integer.parseInt(surroundings.getText()))));

        TextField zoomField = new TextField();
        zoomField.setPrefWidth(30);
        zoomField.setTextFormatter(new TextFormatter<>(new NumberStringConverter()));
        zoomField.setText(zoom + "");
        zoomField.textProperty().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable o) {
                zoom = Double.parseDouble(zoomField.getText());
                zoom = Math.min(5, Math.max(1, zoom));
                for (Node column : chartStack.getChildren()) {
                    for (Node smallChart : ((Pane)column).getChildren()) {
                        ((SmallChart) smallChart).setZoom(zoom);
                    }
                }
            }
        });

        TextField gpioOut = new TextField();
        gpioOut.setPrefWidth(30);
        gpioOut.setTextFormatter(new TextFormatter<>(new NumberStringConverter()));
        gpioOut.setText("4");

        TextField waveInstructions = new TextField();
        waveInstructions.setPrefWidth(1000);
//        waveInstructions.setTextFormatter(new TextFormatter<>(new NumberStringConverter()));
        waveInstructions.setText("1000 2000 1000 2000 1000 1");

        Button sendWaveInstuctions = new Button("send");
        sendWaveInstuctions.setOnAction(event -> {
            String[] split = waveInstructions.getText().split("[^0-9]+");
            outQueue.add(Integer.parseInt(gpioOut.getText()));
            outQueue.add(split.length);
            for (String s : split) {
                if (s.isEmpty()) continue;
                outQueue.add(Integer.parseInt(s));
            }
        });

        chart = new Chart();

        Pane inputs = new HBox(
                new Label(" sensitivity: "), sensitivity,
                new Label(" rapidity: "), rapidity,
                new Label(" surroundings: "), surroundings,
                new Label(" zoom (1-5): "), zoomField,
                new Label(" gpio out: "), gpioOut,
                new Label(" wave instructions: "), waveInstructions,
                sendWaveInstuctions
        );
        chartStack = new FlowPane(Orientation.VERTICAL, new VBox());  // need flow box because HBox not support varialbe height for children
        Pane vBox = new VBox(inputs, chart, chartStack);
        scene = new Scene(vBox, 1670, 800, true, SceneAntialiasing.DISABLED);
        primaryStage.setScene(scene);
        primaryStage.show();
        createUpateTimer();
    }

    private void createUpateTimer() {
        new AnimationTimer() {
            private long last = 0;
            private boolean remove;
            @Override
            public void handle(long now) {
                while (!inQueue.isEmpty()) {
                    Integer value = inQueue.remove();
                    chart.putValue(value, currentChannel);
                    List<Integer> result = analyzers.get(currentChannel).putValue(value);
                    if (result != null) {
                        if (file != null) {
                            try {
                                Files.write(file, (result.toString() + "\n").getBytes("utf-8"), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                        }
                        SmallChart smallChart = new SmallChart();
                        smallChart.putValues(result);
                        smallChart.setZoom(zoom);

                        ObservableList<Node> stack = chartStack.getChildren();
                        if (stack.isEmpty()) {
                            stack.add(new VBox());
                        }
                        Pane column = (Pane) stack.get(stack.size() - 1);
                        if (column.getHeight() + smallChart.getPrefHeight() > scene.getHeight() - chart.getHeight()) {
                            column = new VBox();
                            stack.add(column);
                        }
                        column.getChildren().add(smallChart);
                    }
                    switchChannel();
                }
                long delta = now - last;

                if ((chartStack.getWidth() > scene.getWidth()
                        && chartStack.getChildren().size() > 0
                    ) || remove)
                {
                    remove = true;
                    VBox node = (VBox) chartStack.getChildren().get(0);
                    double width = node.getWidth();
                    width -= delta / 500_000; // cca 2000px/s
                    if (width <= 0) {
                        chartStack.getChildren().remove(0);
                        remove = false;
                    } else {
                        node.setMinWidth(width);
                        node.setPrefWidth(width);
                        node.setMaxWidth(width);
                        last = now;
                    }
                } else {
                    last = now;
                }
            }
        }.start();
    }

    private void switchChannel() {
        currentChannel++;
        if (currentChannel >= channels) currentChannel = 0;
    }

    public static void startSync(Queue<Integer> inQueue, Queue<Integer> outQueue, int channels, Path file) throws InterruptedException {
        Gui.inQueue = inQueue;
        Gui.outQueue = outQueue;
        Gui.channels = channels;
        Gui.file = file;
        Application.launch();
    }

}
