package laser.visualization;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 *
 * @author David
 */
public class Analyzer {

    private Queue<Integer> history = new ArrayDeque<>();
    private Queue<Integer> lastValues = new ArrayDeque<>();

    private int sensivity = 200;
    private int rapidity = 2;
    private int surroundings = 20;

    private boolean detected;
    private int detectedCounter;

    public List<Integer> putValue(int value) {
        addToHisotry(value);
        Integer lastValue = addToLastValues(value);
        if (lastValue != null) {
            if (Math.abs(lastValue - value) >= sensivity) {
                detected = true;
                detectedCounter = 0;
            }
        }
        if (detected) {
            detectedCounter++;
            if (detectedCounter > surroundings) {
                detected = false;
                return new ArrayList<>(history);
            }
        }
        return null;
    }

    private Integer addToLastValues(int value) {
        Integer lastValue = null;
        lastValues.add(value);
        while (lastValues.size() > rapidity) {
            lastValue = lastValues.remove();
        }
        return lastValue;
    }

    private void addToHisotry(int value) {
        history.add(value);
        if (!detected) {
            while (history.size() > surroundings) {
                history.remove();
            }
        }
    }


    public int getSensivity() {
        return sensivity;
    }

    public void setSensivity(int sensivity) {
        this.sensivity = sensivity;
    }

    public int getRapidity() {
        return rapidity;
    }

    public void setRapidity(int rapidity) {
        this.rapidity = rapidity;
    }

    public int getSurroundings() {
        return surroundings;
    }

    public void setSurroundings(int surroundings) {
        this.surroundings = surroundings;
    }


}
