import subprocess

from pylib.coding import Decoder


def read():
    adc = subprocess.Popen(["sudo", "/home/pi/david/reader", "--sample_rate", "3000"], stdout=subprocess.PIPE, bufsize=100)
    adc_out = adc.stdout
    decoder = Decoder(20,50,2)
    try:
        while True:
            v = adc_out.readline()
            result = decoder.put_value(int(v))
            if result != None:
                print("received: {}".format(result))
    except Exception as e:
        print(e)
        raise  # for now nothing


if __name__ == "__main__":
    read()

