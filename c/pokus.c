#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

struct timespec time_s;
int nano_time() {
    clock_gettime(CLOCK_REALTIME, &time_s);
    return time_s.tv_sec * 1000000000 + time_s.tv_nsec;
}

long long nano_time_long() {
    clock_gettime(CLOCK_REALTIME, &time_s);
    return time_s.tv_sec * 1000000000LL + time_s.tv_nsec;
}

int micro_time() {
    clock_gettime(CLOCK_REALTIME, &time_s);
    return time_s.tv_sec * 1000000 + time_s.tv_nsec / 1000;
}

long long micro_time_long() {
    clock_gettime(CLOCK_REALTIME, &time_s);
    return time_s.tv_sec * 1000000LL + time_s.tv_nsec / 1000;
}

int mili_time() {
    clock_gettime(CLOCK_REALTIME, &time_s);
    return  time_s.tv_sec * 1000 + time_s.tv_nsec / 1000000;
}

long long mili_time_long() {
    clock_gettime(CLOCK_REALTIME, &time_s);
    return  time_s.tv_sec * 1000 + time_s.tv_nsec / 1000000;
}

void neco() {

}

void test_func(char * name, void (*fnc)()) {

    int calls = 10;
    // calibrate 
    long long ct = mili_time_long();
    while (mili_time_long() < ct + 10) {
        for (int i = 0; i < calls; i++) {
            (*fnc)();
        }
        if (calls > 10000000) break; // time variable can overflow and then time limit is unusable
        calls += calls / 2;
    }
    calls *= 100;


    for (int i = 0; i < 5; ++i)
    {
        long long st = mili_time_long();
        for (int j = 0; j < calls; ++j)
        {
            (*fnc)();
        }
        long long t = mili_time_long() - st;
        printf("%-32s %10lld ms   %lld ns/call\n", name, t, (t * 1000000LL) / calls);
    }
    printf("\n");
    fflush(stdout);
}



int main(int argc, char *argv[]) {

    test_func("nano_time",       &nano_time);
    test_func("nano_time_long",  &nano_time_long);
    test_func("micro_time",      &micro_time);
    test_func("micro_time_long", &micro_time_long);
    test_func("mili_time",       &mili_time);
    test_func("mili_time_long",  &mili_time_long);

    return 0;
}
