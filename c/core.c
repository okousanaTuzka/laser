/*
 * gcc -Wall -pthread -fPIC -o core core.c -lpigpio
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <signal.h>
#include <fcntl.h>
#include <time.h>

#include <pigpio.h>

#define SPI_SS 8 // GPIO for slave select.

#define ADCS 1    // Number of connected MCP3202.

#define BITS 12            // Bits per reading.
#define BX 8               // Bit position of data bit B11.
#define B0 (BX + BITS - 1) // Bit position of data bit B0.

#define MISO1 9   // ADC 1 MISO.

#define MAX_BUFFER 90              // Generally make this buffer as large as possible
#define MAX_WAVEFORM_TIME 50000     // reading lag and transmiting lag will be slightly bigger then this value, but too low value can cause data lost

// gpio outputs which is possible control by sending istruction to this program
char GPIO_OUTS[] = {4, 5}; // 4 = LED, 5 = LASER
char GPIO_OUTS_C = 2;


// #define REPEAT_MICROS 40 // Reading every x microseconds.

//#define SAMPLES 100000  // Number of samples to take,

int MISO[ADCS]={MISO1};

rawSPI_t rawSPI =
{
   .clk     = 11, // GPIO for SPI clock.
   .mosi    = 10, // GPIO for SPI MOSI.
   .ss_pol  =  1, // Slave select resting level.
   .ss_us   =  0, // Wait 1 micro after asserting slave select.
   .clk_pol =  0, // Clock resting level.
   .clk_pha =  0, // 0 sample on first edge, 1 sample on second edge.
   .clk_us  =  1, // 2 clocks needed per bit so 500 kbps.
};

struct Buffer {
    char *buffer;
    int size;
    int position;
    int low_mark;
    int high_mark;
};

struct WaveInstrucion {
    char output_curr_state;
    int remaining_time;
    int times_c;
    int times_pos;
    int times[];
};

struct Reading {
    int wave_id;
    int topOOL;
    float cbs_per_reading;
};

struct Handler {
    int dma_buffer_size;
    int sample_rate;
    char channels_c;
    char channels[8];
    struct Reading *reading_curr;
    struct Reading *reading_next;
    struct WaveInstrucion *wave_instructions[32];
};


int DEBUG = 0;
int PERF = 0;
int PERF_SUBLVL = 0;

int run_flag = 1;

rawWave_t wave_cache[PI_WAVE_MAX_PULSES];
int wave_cache_c = 0;

int guard(int n, char * err) { if (n == -1) { perror(err); exit(1); } return n; }

int log_err(int n, char * err) { if (n < 0) { fprintf(stderr, "ERROR: error value: %d, msg: %s\n", n, err); } return n; }


char * get_perf_indent() {
    switch(PERF_SUBLVL) {
        case 0:
            return "";
        case 1:
            return "    ";
        case 2:
            return "        ";
        case 3:
            return "            ";
        case 4:
            return "                ";
        case 5:
            return "                    ";
        case 6:
            return "                        ";
        default:
            return "                            ";
    }
}
void log_perf(char * label, long long time) {
    fprintf(stderr, "PERF: %-32s|%s %6lld us\n", label, get_perf_indent(), time);
}
void log_perf_separator() {
    fprintf(stderr, "--------------------------------------------------------------------------------\n");
}

struct timespec time_s;
long long nano_time() {
    clock_gettime(CLOCK_REALTIME, &time_s);
    return time_s.tv_sec * 1000000000LL + time_s.tv_nsec;
}
long long micro_time() {
    clock_gettime(CLOCK_REALTIME, &time_s);
    return time_s.tv_sec * 1000000LL + time_s.tv_nsec / 1000;
}


void set_fd_non_blocking(int fd) {
    int flags = guard(fcntl(fd, F_GETFL), "get flags on file descriptor fail");
    guard(fcntl(fd, F_SETFL, flags | O_NONBLOCK), "set flags fail");
}


void handle_sig(int sig) {
    printf("dont work, dont know why");
    run_flag = 0;
}

int create_waveform(struct Handler *handler) {
    long long t1;
    long long t2;
    long long t3;
    long long t4;
    if (PERF) {
        PERF_SUBLVL++;
        t1 = micro_time();
    }

    if (handler->dma_buffer_size % handler->channels_c) {
        fprintf(stderr, "wrong dma buffer size(%d) for given channels(%d).\n", handler->dma_buffer_size, handler->channels_c);
        return -1;
    }

    gpioPulse_t final[2];

    int repeat_micros = (1000000 / handler->sample_rate);

    gpioWaveAddNew();

    /*
    MCP3202 12-bit ADC 2 channels

    1  2  3  4  5  6   7   8  9  10 11 12 13 14 15 16 17
    SB SD OS MS NA B11 B10 B9 B8 B7 B6 B5 B4 B3 B2 B1 B0

    SB  1  1
    SD  1  0=differential 1=single
    OS  0  0=ch0, 1=ch1 (in single mode)
    MS  0  0=tx lsb first after tx msb first
     */

    /*
       Now construct lots of bit banged SPI reads.  Each ADC reading
       will be stored separately.  We need to ensure that the
       buffer is big enough to cope with any reasonable rescehdule.

       In practice make the buffer as big as you can.
     */

    if (PERF) {
        t2 = micro_time();
    }

    int wave_end = 0;
    if (wave_cache_c) {
        log_err(rawWaveAddGeneric(wave_cache_c, wave_cache), "Fail add waves from cache.");
        wave_end = gpioWaveGetMicros();
    } else {
        char buf[2];
        for (int i = 0; i < handler->dma_buffer_size; i += handler->channels_c) {
            for (int j = 0; j < handler->channels_c; j++) {
                buf[0] = 0xC0 | (handler->channels[j] << 3) ; // Start bit, single ended, channel 0.
                rawWaveAddSPI(&rawSPI, wave_end, SPI_SS, buf, 5, BX, B0, B0);
                /*
                   REPEAT_MICROS must be more than the time taken to
                   transmit the SPI message.
                 */
                wave_end += repeat_micros;
            }
        }

        // Force the same delay after the last reading.
        final[0].gpioOn = 0;
        final[0].gpioOff = 0;
        final[0].usDelay = wave_end;

        final[1].gpioOn = 0; // Need a dummy to force the final delay.
        final[1].gpioOff = 0;
        final[1].usDelay = 0;

        gpioWaveAddGeneric(2, final);

        if (log_err(rawWaveCopyCurrent(PI_WAVE_MAX_PULSES, wave_cache), "Coping to cache fail.") == 0) {
            wave_cache_c = rawWaveCurrentSize();

            if (DEBUG) {
                fprintf(stderr, "DEBUG: cached waves:\n");
                for (int i = 0; i < wave_cache_c; i++) {
                    fprintf(stderr, "        tgpioOn: %d    gpioOff: %d    usDelay: %d    flags: %d\n",   wave_cache[i].gpioOn, wave_cache[i].gpioOff, wave_cache[i].usDelay, wave_cache[i].flags);
                }
            }
        }

    }

    if (PERF) {
        t3 = micro_time();
    }
    // add aditional signal
    for (int gpio = 0; gpio < 32; gpio++) {
        if (handler->wave_instructions[gpio]) {
            if (DEBUG) fprintf(stderr, "DEBUG: creating wave for transmiting gpio_out: %d\n", gpio);

            struct WaveInstrucion *wave_instruction = handler->wave_instructions[gpio];
            int available_time = wave_end;
            int additional_counter = 0;
            gpioPulse_t additional_signal[wave_instruction->times_c + 2];

            if (DEBUG) {
                fprintf(stderr, "DEBUG: creating wave output_curr_state: %6d remaining_time: %6d times_pos: %6d\n", wave_instruction->output_curr_state, wave_instruction->remaining_time, wave_instruction->times_pos);
            }

            if (wave_instruction->remaining_time < available_time) {
                // add remaing time from last waveform as delay
                additional_signal[additional_counter].gpioOff = 0;
                additional_signal[additional_counter].gpioOn  = 0;
                additional_signal[additional_counter].usDelay = wave_instruction->remaining_time;

                // set times for currently added wave part
                available_time -= wave_instruction->remaining_time;
                wave_instruction->remaining_time = 0;

                additional_counter++;
            } else {
                // decreace remaing time by unused time from current waveform
                wave_instruction->remaining_time -= available_time;
                available_time = 0;
            }

            while (wave_instruction->times_pos < wave_instruction->times_c && wave_instruction->remaining_time < available_time) {

                // little trick, using output_curr_state mark as value for binary shift save ugly if-else block and can be faster
                additional_signal[additional_counter].gpioOff = wave_instruction->output_curr_state << gpio;
                wave_instruction->output_curr_state           = 1 - wave_instruction->output_curr_state;
                additional_signal[additional_counter].gpioOn  = wave_instruction->output_curr_state << gpio;

                // wave instruction is longer then available time
                if (wave_instruction->times[wave_instruction->times_pos] < available_time) {
                    additional_signal[additional_counter].usDelay = wave_instruction->times[wave_instruction->times_pos];
                    available_time -= wave_instruction->times[wave_instruction->times_pos];
                } else {
                    additional_signal[additional_counter].usDelay = 1;
                    // decreace remaing time by unused time from current waveform
                    wave_instruction->remaining_time = wave_instruction->times[wave_instruction->times_pos] - available_time;
                    available_time = 0;
                }

                additional_counter++;
                wave_instruction->times_pos++;
            }

            if (wave_instruction->times_pos == wave_instruction->times_c && wave_instruction->remaining_time <= 0) {
                if (DEBUG) fprintf(stderr, "DEBUG: wave instruction is whole processed, removing.\n");
                free(handler->wave_instructions[gpio]);
                handler->wave_instructions[gpio] = NULL;
            }

            log_err(gpioWaveAddGeneric(additional_counter, additional_signal), "981897 fail add wave");
        }
    }
    if (PERF) {
        t4 = micro_time();
    }

    // Force the same delay after the last reading.
//    final[0].gpioOn = 0;
//    final[0].gpioOff = 0;
//    final[0].usDelay = wave_end;

//    final[1].gpioOn = 0; // Need a dummy to force the final delay.
//    final[1].gpioOff = 0;
//    final[1].usDelay = 0;

//    gpioWaveAddGeneric(2, final);


    // WARN: BOOL and TOOL share same memory capacity
    int wave_id = gpioWaveCreatePad(50, 25, 25); // Create the wave from added data.
    if (wave_id < 0) {
        fprintf(stderr, "Can't create wave, error %d, dma_buffer_size %d. Maybe too many wave instructions.\n", wave_id, handler->dma_buffer_size);
        return -1;
    }
    if (DEBUG) fprintf(stderr, "DEBUG: New wave created %d \n",  wave_id);
    if (PERF) {
        long long end_time = micro_time();
        log_perf("creating new wave init:", t2 - t1);
        log_perf("creating new wave reading:", t3 - t2);
        log_perf("creating new wave sending:", t4 - t3);
        log_perf("creating new wave final:", end_time - t4);
        PERF_SUBLVL--;
    }
    return wave_id;
}


struct Handler * Handler_init(int dma_buffer_size, int sample_rate, char channels_c, char channels[]) {

    struct Handler *handler = malloc(sizeof(*handler));
    handler->dma_buffer_size = dma_buffer_size;
    handler->sample_rate = sample_rate;
    handler->channels_c = channels_c;
    memmove(&handler->channels, channels, sizeof(char) * channels_c);
    handler->reading_curr = NULL;
    handler->reading_next = NULL ;
    memset(handler->wave_instructions, 0, sizeof(handler->wave_instructions));

    return handler;
}


struct Reading * Reading_init(int wave_id) {

    /*
       The wave resources are now assigned,  Get the number
       of control blocks (CBs) so we can calculate which reading
       is current when the program is running.
     */
    rawWaveInfo_t rwi = rawWaveInfo(wave_id);

    /*
       OOL are allocated from the top down. There are BITS bits
       for each ADC reading and BUFFER ADC readings.  The readings
       will be stored in topOOL - 1 to topOOL - (BITS * BUFFER).
     */
    int topOOL = rwi.topOOL;

    struct Reading* reading;
    reading = malloc(sizeof(*reading));
    reading->wave_id = wave_id;
    reading->topOOL = topOOL;

    return reading;
}

struct WaveInstrucion * WaveInstrucion_init(int gpio_out, int wave_times_c, int wave_times[]) {

    struct WaveInstrucion *wave_instruction = malloc(sizeof(*wave_instruction) + sizeof(int) * wave_times_c);
    wave_instruction->output_curr_state = 0;
    wave_instruction->remaining_time = 0;
    wave_instruction->times_c = wave_times_c;
    wave_instruction->times_pos = 0;
    memmove(&wave_instruction->times, wave_times, sizeof(int) * wave_times_c);

    if (DEBUG) {
        fprintf(stderr, "DEBUG: New wave Instructions:\n");
        fprintf(stderr, "DEBUG:         wave_instruction->output_curr_state: %6d \n", wave_instruction->output_curr_state);
        fprintf(stderr, "DEBUG:         wave_instruction->remaining_time:    %6d \n", wave_instruction->remaining_time);
        fprintf(stderr, "DEBUG:         wave_instruction->times_c:           %6d \n", wave_instruction->times_c);
        fprintf(stderr, "DEBUG:         wave_instruction->times_pos:         %6d \n", wave_instruction->times_pos);

        for (int i = 0; i < wave_instruction->times_c; i++) {
            fprintf(stderr, "DEBUG:         wave_instruction->times:             %6d \n", wave_instruction->times[i]);
        }
    }

    return wave_instruction;
}

/*
   This function extracts the MISO bits for each ADC and
   collates them into a reading per ADC.
*/

void get_reading(
        int adc_count, // Number of attached ADCs.
        int *MISO, // The GPIO connected to the ADCs data out.
        int OOL, // Address of first OOL for this reading.
        int bytes, // Bytes between readings.
        int bits, // Bits per reading.
        char *buf)
{
    unsigned int level;
    int p;
    int adc_index;
    int bit_index;

    p = OOL;

    for (bit_index = 0; bit_index < bits; bit_index++) {
        level = rawWaveGetOut(p);
        for (adc_index = 0; adc_index < adc_count; adc_index++) {
            putBitInBytes(bit_index, buf + (bytes * adc_index), level & (1 << MISO[adc_index]));
        }
        p--;
    }
}

void Handler_internal_read(struct Reading* reading_data, struct Buffer* buffer, int dma_buffer_size) {

    long long start_t;
    if (PERF) {
        PERF_SUBLVL++;
        start_t = micro_time();
    }

    char little_buffer[ADCS * 2]; // number of adc * 2
    short int val;
    // Loop gettting the fresh readings.
    for (int reading = 0; reading < dma_buffer_size; reading++) {
        /*
           Each reading uses BITS OOL.  The position of this readings
           OOL are calculated relative to the waves top OOL.
         */
        get_reading(ADCS, MISO, reading_data->topOOL - (reading * BITS) - 1, 2, BITS, little_buffer);
        for (int i = 0; i < ADCS; i++) {
            //   7   6  5  4  3  2  1  0 |  7  6  5  4  3  2  1  0
            // B11 B10 B9 B8 B7 B6 B5 B4 | B3 B2 B1 B0  X  X  X  X */
            val = (little_buffer[i * 2] << 4) | (little_buffer[(i * 2) + 1] >> 4);
            if (buffer->position < buffer->size - 2) {
                memcpy(buffer->buffer + buffer->position, &val, 2);
                buffer->position += 2;
            } else {
                fprintf(stderr, "9810948, buffer is full, position: %d, size: %d.\n",
                        buffer->position, buffer->size);
            }
        }
    }

    if (PERF) {
        log_perf("read from dma buffer:", micro_time() - start_t);
        PERF_SUBLVL--;
    }
}


void Handler_update(struct Handler* handler, struct Buffer* buffer) {

    long long start_t;
    if (PERF) {
        PERF_SUBLVL++;
        start_t = micro_time();
    }

    int curr_wf = gpioWaveTxAt();

    if (DEBUG) fprintf(stderr, "DEBUG: transmiting wave %d \n", curr_wf);

    if (handler->reading_curr && (curr_wf == 9998 || curr_wf == 9999)) {
        log_err(-1, "shit happen, no waveform currently transmited = DATA LOST!!!");
    }

    if ((handler->reading_next && curr_wf == handler->reading_next->wave_id) || (curr_wf == 9998 || curr_wf == 9999)) {
        // at startup there isn't reading_curr
        if (handler->reading_curr) {
            Handler_internal_read(handler->reading_curr, buffer, handler->dma_buffer_size);

            gpioWaveDelete(handler->reading_curr->wave_id);
            free(handler->reading_curr);
        }
        handler->reading_curr = handler->reading_next;
        handler->reading_next = NULL;
    }

    if (handler->reading_next == NULL) {
        int new_wf = create_waveform(handler);
        if (new_wf < 0) {
            fprintf(stderr, "Creating waveform fail with %d.\n", new_wf);
        } else {
            handler->reading_next = Reading_init(new_wf);
            if (DEBUG) fprintf(stderr, "DEBUG: set sync switching to wave %d \n", handler->reading_next->wave_id);
            gpioWaveTxSend(handler->reading_next->wave_id, PI_WAVE_MODE_ONE_SHOT_SYNC);
        }
    }

    if (PERF) {
        log_perf("Handler_update:", micro_time() - start_t);
        PERF_SUBLVL--;
    }
}

void Handler_try_add_wave_instruction(struct Handler* handler, struct Buffer* buffer) {
    int bytes = buffer->high_mark - buffer->position;
    if (bytes > 8) {  // fist int is gpio_out and second int is array size
        int *gpio = (int *) (buffer->buffer + buffer->position);
        int *array_size = (int *) (buffer->buffer + buffer->position + 4);
        if (DEBUG) fprintf(stderr, "DEBUG: Receive new wave instructions gpio_out: %d array_size: %d\n", *gpio, *array_size);
        int all_bytes_size = (*array_size) * 4 + 8;
        if (bytes >= all_bytes_size) {
            if (DEBUG) fprintf(stderr, "DEBUG: New wave instructions is complete.\n");
            struct WaveInstrucion *w_inst = WaveInstrucion_init(*gpio, *array_size, array_size + 1);
            if (handler->wave_instructions[*gpio] != NULL) {
                // overriding old wave instruction, need first free memory
                free(handler->wave_instructions[*gpio]);
            }
            handler->wave_instructions[*gpio] = w_inst;
            // mark byte as readed
            buffer->position += all_bytes_size;
        }
    }
}


void read_to_buffer(int fd, struct Buffer* buffer) {
    if (buffer->position == buffer->size) {
        log_err(-1, "read_to_buffer(): buffer full");
        return;
    }
    int r = read(fd, buffer->buffer + buffer->position, buffer->high_mark - buffer->position);
    if (r > 0) {
        buffer->position += r;
    }
}

void buffer_flip_to_read(struct Buffer* buffer) {
    buffer->high_mark = buffer->position;
    buffer->position = buffer->low_mark;
}

void buffer_flip_to_write(struct Buffer* buffer) {
    int contain_bytes = buffer->high_mark - buffer->position;
    if (contain_bytes) {
        memmove(buffer->buffer, buffer->buffer + buffer->position, contain_bytes);
    }
    buffer->low_mark = 0;
    buffer->high_mark = buffer->size;
    buffer->position = contain_bytes;
}

void buffer_clean(struct Buffer* buffer) {
    buffer->position = 0;
    buffer->low_mark = 0;
    buffer->high_mark = buffer->size;
}


void print_help_and_exit() {
    printf("read from ADC and numbers write to stdout - each number have 2 bytes in little endian order\n");
    printf("from stdin read values (all values are 4 bytes int in little endian order) for control gpio outputs\n");
    printf("    first number - specify gpio output\n");
    printf("    second number - say how many number represent output signal will follow\n");
    printf("    n mubers - time(in us) between switch output state\n");
    printf("    - 5, 4, 100, 300, 200, 1 mean - use gpio 5\n");
    printf("                                    sending 4 number\n");
    printf("                                    turn on and wait 100us, then\n");
    printf("                                    turn off and wait 300us, then\n");
    printf("                                    turn on and wait 200us, then\n");
    printf("                                    turn off and wait 1us, just for off, otherwise out stay on forever\n");
    printf("\n");
    printf("arguments:\n");
    printf("    --sample_rate [1-25000]        default: 1000 - samples per second\n");
    printf("    --channels    [0-7 .. [0-7]]   default: 0    - ADC channel for reading, current 0 = from led, 1 = from laser\n");
    printf("    --debug                                      - debug output to stderr\n");
    printf("    --perf                                       - performance output to stderr\n");
    exit(0);
}

int main(int argc, char *argv[]) {
    int sample_rate = 100;
    char channels_c = 0;
    char channels_v[8];

    // supported arguments
    char help_arg[] = "--help";
    char perf_arg[] = "--perf";
    char debug_arg[] = "--debug";
    char sample_rate_arg[] = "--sample_rate";
    char channels_arg[] = "--channels";

    char (*last_arg)[] = 0;
    for (int arg_index = 1; arg_index < argc; arg_index++) {

        // try match argument
        if (strcmp(argv[arg_index], help_arg) == 0) {
            print_help_and_exit();
        } else if (strcmp(argv[arg_index], debug_arg) == 0) {
            last_arg = &debug_arg;
            DEBUG = 1;
        } else if (strcmp(argv[arg_index], perf_arg) == 0) {
            last_arg = &perf_arg;
            PERF = 1;
        } else if (strcmp(argv[arg_index], sample_rate_arg) == 0) {
            last_arg = &sample_rate_arg;
        } else if (strcmp(argv[arg_index], channels_arg) == 0) {
            last_arg = &channels_arg;

        // if not match argument, try hadle it as value
        } else if (*last_arg && strcmp(*last_arg, sample_rate_arg) == 0) {
            sample_rate = atoi(argv[arg_index]);
            if (sample_rate < 1 || sample_rate > 25000) {
                fprintf(stderr, "invalid %s value %s\n", sample_rate_arg, argv[arg_index]);
                print_help_and_exit();
            }
        } else if (*last_arg && strcmp(*last_arg, channels_arg) == 0) {
            char ch = (char) atoi(argv[arg_index]);
            if (ch < 0 || ch > 7) {
                fprintf(stderr, "invalid %s value %s\n", channels_arg, argv[arg_index]);
                print_help_and_exit();
            }
            channels_v[(int)channels_c] = ch;
            channels_c++;
        } else {
            fprintf(stderr, "invalid value(%s) for %s \n", argv[arg_index], *last_arg);
            print_help_and_exit();
        }
    }
    // no channel, setup default
    if (channels_c == 0) {
        channels_v[0] = 0;
        channels_c++;
    }

    guard(gpioInitialise(), "fail gpio initialise\n");
    // Need to set GPIO as outputs otherwise wave will have no effect.
    gpioSetMode(rawSPI.clk, PI_OUTPUT);
    gpioSetMode(rawSPI.mosi, PI_OUTPUT);
    gpioSetMode(SPI_SS, PI_OUTPUT);
    gpioSetMode(MISO1, PI_INPUT);
    for (int i = 0; i < GPIO_OUTS_C; i++) {
        gpioSetMode(GPIO_OUTS[i], PI_OUTPUT);
    }
    log_err(gpioWrite(SPI_SS, rawSPI.ss_pol), "Cant switch on SPI_SS");

    // INIT stdin reading buffer
    set_fd_non_blocking(0); // 0 = stdin
    int in_read_buffer_size = 4096;
    char b[in_read_buffer_size];
    struct Buffer in_read_buffer = {b, in_read_buffer_size, 0, 0, in_read_buffer_size};
    buffer_clean(&in_read_buffer);

    // how much reading fit to time limit
    int dma_buffer_size = MAX_WAVEFORM_TIME / (1000000 / sample_rate);
    // buffer size must be multiple of used channels
    dma_buffer_size = (dma_buffer_size / channels_c) * channels_c;
    if (dma_buffer_size == 0) {
        dma_buffer_size = channels_c;
        fprintf(stderr, "Can't fit to MAX_WAVEFORM_TIME(%d us), update loop will be longer.\n", MAX_WAVEFORM_TIME);
    }
    // buffer cant be bigger then MAX_BUFFER (typicaly memory limit)
    if (dma_buffer_size > MAX_BUFFER) {
        dma_buffer_size = (MAX_BUFFER / channels_c) * channels_c;
    }
    if (dma_buffer_size == 0) {
        fprintf(stderr, "Fail create dma buffer for reading. MAX_BUFFER %d MAX_WAVEFORM_TIME %d channels %d \n", MAX_BUFFER, MAX_WAVEFORM_TIME, channels_c);
        return -1;
    }

    int single_reading_time = (1000000 / sample_rate) * dma_buffer_size;
    // time between updates, must be shorter then time of reading!!!
    int target_update_time = single_reading_time / 4;

    // Handler_init(int dma_buffer_size, int sample_rate, char channels_c, char *channels[])
    // INIT Handler
    struct Handler* handler;
    handler = Handler_init(dma_buffer_size, sample_rate, channels_c, channels_v);
    if (handler == 0) {
        run_flag = 0;
        fprintf(stderr, "fail init Handler\n");
    }
    // teoretical maximum = dma_buffer_size * number of adc * adc output bytes * (2 switching waveforms)
    int reader_buff_size = dma_buffer_size * ADCS * 2 * 2;
    char rb[reader_buff_size];
    struct Buffer handler_buffer = {rb, reader_buff_size, 0, 0, reader_buff_size};
    buffer_clean(&handler_buffer);

    long long start_t;
    long long end_t;
    long long update_time;
    int target_sleep_time;
    long long sleep_time;
    while (run_flag) {
        start_t = micro_time();

        Handler_update(handler, &handler_buffer);
        buffer_flip_to_read(&handler_buffer);
        write(1, handler_buffer.buffer + handler_buffer.position, handler_buffer.high_mark - handler_buffer.position); // 1 = stdout
        buffer_clean(&handler_buffer);

        read_to_buffer(0, &in_read_buffer);
        buffer_flip_to_read(&in_read_buffer);
        Handler_try_add_wave_instruction(handler, &in_read_buffer);
        buffer_flip_to_write(&in_read_buffer);

        end_t = micro_time();
        update_time = end_t - start_t;
        if (update_time > single_reading_time) {
            fprintf(stderr, "ERROR: update_loop duration (%lld) was longer then time of single reading (%d)\n", update_time, single_reading_time);
        }
        target_sleep_time = target_update_time - update_time;
        if (target_sleep_time < 0 || target_sleep_time > target_update_time) target_sleep_time = 0; // probably overflow some variable, skip sleep just for safe
        usleep(target_sleep_time);
        sleep_time = micro_time() - end_t;
        if (DEBUG) fprintf(stderr, "DEBUG: ---------- Update loop end. update time: %lldus, sleep_time: %lldus target_sleep_time: %dus target_update_time: %dus ----------\n",  update_time, sleep_time, target_sleep_time, target_update_time);
        if (PERF) { log_perf("update_loop:", update_time); log_perf_separator(); }
    }

    gpioTerminate();
    printf("end\n");

    return 0;
}