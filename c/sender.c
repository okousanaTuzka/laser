/*
 * gcc -Wall -pthread -fPIC -shared -o sender sender.c -lpigpio
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>

#include <pigpio.h>

int send(int gpio, int wave_count, int *wave_times) {
    int wid;
    
    wave_count++;
    gpioPulse_t pulses[wave_count];

    if (gpioInitialise() < 0) {
        printf("fail gpio initialise\n");
        return -1;
    }

    gpioSetMode(gpio, PI_OUTPUT);

    int i;
    int wave_time;
    
    // turn on before add coded signal
    pulses[0].gpioOn = (1 << gpio);
    pulses[0].gpioOff = 0;
    pulses[0].usDelay = wave_times[0];
    int gpio_state = 1;
    
    for (i = 1; i < wave_count; i++) {
        wave_time = wave_times[i];
        if (gpio_state) {
            pulses[i].gpioOn = 0;
            pulses[i].gpioOff = (1 << gpio);
            gpio_state = 0;
        } else {
            pulses[i].gpioOn = (1 << gpio);
            pulses[i].gpioOff = 0;
            gpio_state = 1;
        }
        pulses[i].usDelay = wave_time;
    }


    //gpioWaveClear();

    gpioWaveAddGeneric(wave_count, pulses);

    wid = gpioWaveCreate();

    if (wid >= 0) {
        gpioWaveTxSend(wid, PI_WAVE_MODE_ONE_SHOT);

        while (gpioWaveTxBusy()) {
            usleep(1000);
        }
    }

    gpioTerminate();
    
    return 0;
}

int main(int argc, char *argv[]) {
    int debug = 0;

    int i;
    if (argc > 1) {
        for (i = 1; i < argc; i++) {
            if (strcmp(argv[i], "--debug") == 0) {
                debug = 1;
            }
        }
    }

    printf("run\n");
    
    int waves[11] = {1000, 1000, 1000, 1000, 1000, 3000, 1000, 1000, 1000, 1000, 1000};
    
    send(4, 11, waves);
    
    printf("end\n");
    
    return 0;
}