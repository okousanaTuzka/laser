import signal
import subprocess
import time

from pylib.coding import Decoder

from gpiozero import LED


def read():
    led = LED(4)
    laser = LED(5)
    adc = subprocess.Popen(["sudo", "/home/pi/core", "--sample_rate", "3000", "--channel", "1"], stdout=subprocess.PIPE, bufsize=100)
    adc_out = adc.stdout
    decoder = Decoder(6, 20, 2)
    try:
        while True:
            bytes = adc_out.read(2)
            result = decoder.put_value(int.from_bytes(bytes, "little"))
            if result != None:
                print("hit")
                led.on()
                laser.on()
                time.sleep(0.3)
                led.off()
                laser.off()


    except Exception as e:
        adc.kill()
        adc.wait()
        print(e)
        raise  # for now nothing


if __name__ == "__main__":
    read()

