import tkinter
from turtle import Canvas

from pylib.utils import CycleQueue


class Graph(object):

    def __init__(self):
        self.width=1800
        self.height=1024
        self.queue = CycleQueue(self.width, 0)
        self.root = tkinter.Tk()
        self.canvas = Canvas(self.root, width=self.width, height=self.height)
        self.canvas.pack()
        self.points = []
        for value in self.queue.values():
            self.points.append(self.canvas.create_rectangle(0, 0, 1, 1))
        self.line = self.canvas.create_line(self.queue.get_index(), 0, self.queue.get_index(), self.height)

    def put_values(self, values):
        for v in values:
            y = max(0, self.height - (v // 4))
            point = self.points[self.queue.get_index()]
            x = self.queue.get_index()
            self.canvas.coords(point, x, y, x+1, y+1)
            self.queue.put(v)
        self.canvas.coords(self.line, self.queue.get_index(), 0, self.queue.get_index(), self.height)
        self.root.update()