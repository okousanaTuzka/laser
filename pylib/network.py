import socket
import time
from queue import Empty
from queue import Queue
from threading import Thread

from pylib.visualization import Graph


class Client(object):

    def __init__(self, host, port):
        self.host = host
        self.port = port

    def start(self, callback):
        print("client start")
        while True:
            try:
                with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                    s.settimeout(5)
                    s.connect((self.host, self.port))
                    while True:
                        bytes_ = s.recv(2)
                        if bytes_:
                            callback(int.from_bytes(bytes_, "big"))
                        else:
                            time.sleep(0.01)
            except Exception as e:
                print(e)
                print("reconnect")


class BroadcastServer(object):

    def __init__(self, port):
        self.port = port
        self.run_flag = False
        self.thread = None
        self.clients = []
        self.queue = Queue(10000)

    def start(self):
        self.run_flag = True
        self.thread = Thread(target=self._run)
        self.thread.start()

    def stop(self):
        self.run_flag = False

    def send_data(self, value):
        self.queue.put(value, block=False)

    def _run(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind(("", self.port))
            s.setblocking(False)
            s.listen(5)
            while self.run_flag:
                # handle new connection
                while True:
                    conn = addr = None
                    try:
                        conn, addr = s.accept()
                    except BlockingIOError:
                        pass
                    if conn:
                        print('Connected by', addr)
                        self.clients.append(conn)
                    else:
                        break
                # send data to clients
                data = bytearray()
                for i in range(65000):
                    try:
                        value = self.queue.get(block=False)
                        data.extend(value.to_bytes(2, "big"))
                    except Empty:
                        time.sleep(0.01)
                        break
                if data:
                    for_remove = []
                    for client in self.clients:
                        try:
                            conn, addr = s.accept()
                        except BlockingIOError:
                            pass
                        if conn:
                            print('Connected by', addr)
                            self.clients.append(conn)
                        else:
                            break
                    # send data to clients
                    data = bytearray()
                    while True:
                        try:
                            value = self.queue.get(block=False)
                            data.extend(value.to_bytes(2, "big"))
                        except Empty:
                            break
                    if data:
                        for_remove = []
                        for client in self.clients:
                            try:
                                client.sendall(data)
                            except Exception:
                                for_remove.append(client)
                        for client in for_remove:
                            self.clients.remove(client)
                    time.sleep(0.1)
        finally:
            for client in self.clients:
                client.close()
