

class CycleQueue(object):
    def __init__(self, size, fill_with_value=None):
        self.queue = [fill_with_value] * size
        self.index = 0
        self.size = size

    def get_index(self):
        return self.index

    def get_last(self):
        return self.queue[self.index - 1]

    def put(self, value):
        self.queue[self.index] = value
        self.index += 1
        if self.index == self.size:
            self.index = 0

    def values(self):
        return self.queue
