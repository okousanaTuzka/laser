
from collections import deque
from typing import List


class Const(object):
    ENCODING_POSITIVE = 1
    ENCODING_NEGATIVE = -1
    ZERO = 0.001
    ONE = 0.003


class Decoder(object):

    def __init__(self, max_period, sensitivity=10, rapidity=1, encoding=Const.ENCODING_POSITIVE):
        self.max_period = max_period
        self.sensitivity = sensitivity
        self.rapidity = rapidity  # not implemented
        self.encoding = encoding
        self.last_values = deque([0], maxlen=self.rapidity)
        self.last_change = 0
        self.counter = 0
        self.result = []

    def put_value(self, value: int) -> int:
        decoded = None
        self.counter += 1
        if self.counter > self.max_period:
            self.last_change = 0
            if len(self.result) > 3:
                decoded = self.decode()
            self.result = []
        diff = value - self.last_values[0]
        if diff >= self.sensitivity:
            if self.last_change == -1 and (self.encoding == Const.ENCODING_NEGATIVE or self.result):
                self.result.append(self.counter)
            self.last_change = 1
            self.counter = 0
        elif -diff >= self.sensitivity:
            if self.last_change == 1 and (self.encoding == Const.ENCODING_POSITIVE or self.result):
                self.result.append(self.counter)
            self.last_change = -1
            self.counter = 0
        self.last_values.append(value)
        return decoded

    def decode(self):
        value = 0
        zero = 0
        for x in self.result[::2]:
            zero += x
        zero /= len(self.result)
        zero *= 2
        bits = list(map(lambda x: x > zero, self.result[1::2]))
        for bit_i in range(len(bits)):
            if bits[bit_i]:
                value += 1 << bit_i
        return value


class Encoder(object):

    def __init__(self, bits: int):
        assert 0 < bits < 31
        self.bits = bits

    def encode(self, x) -> List[float]:
        result = []
        for bit in range(self.bits):
            result.append(Const.ZERO)
            result.append({True: Const.ONE, False: Const.ZERO}[bool(x & (1 << bit))])
        result.append(Const.ZERO)
        return result