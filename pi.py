import time
import subprocess
from typing import Union, List
from collections import deque

from pylib.coding import Decoder, Encoder


def test():
    a = [0,0,11,11,0,0,0,0,11,11,0,0,11,11,0,0,0,0,0,0]
    d = Decoder(4)
    for x in a:
        y = d.put_value(x)
        if y: print(y)
    print(d.result)

    d = Decoder(30)

    e = Encoder(5)
    print(e.encode(1))
    assert e.encode(1) == [0.001, 0.002, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001]
    print(e.encode(5))
    assert e.encode(5) == [0.001, 0.002, 0.001, 0.001, 0.001, 0.002, 0.001, 0.001, 0.001, 0.001]


if __name__ == "__main__":
    test()
