import socket
import time
import argparse
import subprocess
from collections import deque
from queue import Queue, Empty
from threading import Thread
from fcntl import fcntl, F_GETFL, F_SETFL
from os import O_NONBLOCK

from pylib.network import BroadcastServer


def set_non_blocking(pipe):
    flags = fcntl(pipe, F_GETFL)  # get current p.stdout flags
    fcntl(pipe, F_SETFL, flags | O_NONBLOCK)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--sample_rate", default=300)
    parser.add_argument("--channel", default=0)
    parser.add_argument("--gpio_out", default=4)
    args = parser.parse_args()

    bs = BroadcastServer(5013)
    bs.start()

    adc = subprocess.Popen(["sudo", "/home/pi/david/c/core",
                            "--sample_rate", str(args.sample_rate),
                            "--channel", str(args.channel),
                            "--gpio_out", str(args.gpio_out),
                            "--debug"],
                           stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, bufsize=2000)


    adc_out = adc.stdout
    adc_err = adc.stderr
    set_non_blocking(adc_err)
    try:
        while True:
            v = int.from_bytes(adc_out.read(2), "little")
            bs.send_data(v)
            try:
                e = adc_err.readline()
                if e:
                    print("Error: " + str(e))
            except Exception as e:
                print(e)
    finally:
        print("stopping all")
        bs.stop()
        adc.terminate()
