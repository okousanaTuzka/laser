// preview quality:
$fn = 30;


//cube([20,20,0.1], true);

translate([1.9,0,0])
diode(2);

rotate([0,0,120])
translate([1.9,0,0])
diode(2);

rotate([0,0,240])
translate([1.9,0,0])
diode(2);

translate([10,0,0])

mad();

module mad() {
    polyhedron(
    [
        [0,0,0],
        [0,1,0],
        [0,1,1],
    ],
    [
        [],
    ]
    );
};

translate([20,0,0]) {
    
    translate([-0.25,-0.25,0])
    cube([0.5, 0.5, 6]);
    
    translate([1.7,0,4])
    rotate([0,60,0]) 
    smd_diode();

    
    rotate([0,0,120]) 
    translate([1.7,0,4])
    rotate([0,60,0]) 
    smd_diode();
    
    rotate([0,0,240]) 
    translate([1.7,0,4])
    rotate([0,60,0]) 
    smd_diode();
}


module smd_diode() {
    translate([0.15-1.6, -1.4, 0]) {
        translate([-0.15, 0.3, 0])
        cube([3.5, 2.2, 1.1]);
        difference() {
            cube([3.2, 2.8, 1.9]);
            translate([1.6, 1.4, 1.1])
            cylinder(0.9,0.6,1.2);
        }
    }
};

module diode(up_part) {
legs(10);
    
translate([-0.1,0,9.4])
rotate([0,30,0])
legs(1.3);

translate([0,0,10])
rotate([0,60,0])
legs(up_part );
    
translate([0,0,10])
rotate([0,60,0])
translate([0,0,up_part])
cap();
};

module legs(x) {
translate([-1.27-0.25,0-0.25,0])
    cube([0.5,0.5,x]);
translate([1.27-0.25,0-0.25,0])
    cube([0.5,0.5,x]);
};

module cap() {
translate([0, 0, 0])
    cylinder(1, 2.9, 2.9);
translate([0, 0, 1])
    cylinder(4.3, 2.5, 2.5);
};