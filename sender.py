from gpiozero import LED
import time
import random
import pigpio

from pylib.coding import Encoder, Const


class Sender(object):

    def __init__(self, led, encoder: Encoder, encoding=Const.ENCODING_POSITIVE):
        self.led = led
        self.encoder = encoder
        self.encoding = encoding

    def send(self, value):
        encoded = self.encoder.encode(value)
        self.led.on()
        if self.encoding == Const.ENCODING_NEGATIVE:
            time.sleep(Const.ONE)
            self.led.off()
        for x in encoded:
            time.sleep(x)
            self.led.toggle()


class Toggler(object):
    OFF, ON = 0, 1

    def __init__(self, gpio, init_state=OFF):
        self.gpio = gpio
        self.last_state = init_state

    def toggle(self):
        if self.last_state == Toggler.ON:
            self.last_state = Toggler.OFF
            return 0, 1 << self.gpio
        else:
            self.last_state = Toggler.ON
            return 1 << self.gpio, 0


if __name__ == "__main__":
    led = LED(4)
    sender = Sender(led, Encoder(5))
    while True:
        time.sleep(2)
        x = random.randint(0,30)
        print("sending: {}".format(x))
        sender.send(x)
