import time
from queue import Queue, Empty
from threading import Thread

from pylib.network import Client
from pylib.visualization import Graph


def create_client_callback(queue):
    def callback(value):
        queue.put(value, block=False)
    return callback


if __name__ == "__main__":

    c = Client("shootpi", 5013)
    queue = Queue()
    t = Thread(target=lambda: c.start(create_client_callback(queue)), daemon=True)
    t.start()

    g = Graph()
    batch_size = 0
    avg_queue_size = 5000
    st = time.time()
    while True:
        values = []
        for i in range(batch_size):
            try:
                values.append(queue.get(block=False))
            except Empty:
                pass
        queue_size = queue.qsize()
        avg_queue_size += (queue_size - avg_queue_size) / 200
        batch_size = int(avg_queue_size / 100)
        # print("{:5d}  {:5d}".format(queue_size, batch_size))
        g.put_values(values)
        now = time.time()
        time.sleep(max(0., 0.016 - (now - st)))
        st = now
