import random
import time
from blist import blist

if __name__ == "__main__":
    test_data = []
    for i in range(30000):
        test_data.append(random.randint(0, 4095))

    converted = b''
    st = time.perf_counter()
    for x in test_data:
        converted += x.to_bytes(2, "big")
    et = time.perf_counter()
    print("time {:8f} s - concatenate byte string".format(et - st))


    converted = []
    st = time.perf_counter()
    for x in test_data:
        converted.extend(x.to_bytes(2, "big"))
    et = time.perf_counter()
    print("time {:8f} s - extend list".format(et - st))

    converted = bytearray()
    st = time.perf_counter()
    for x in test_data:
        converted.extend(x.to_bytes(2, "big"))
    et = time.perf_counter()
    print("time {:8f} s - extend bytearray".format(et - st))

    converted = blist()
    st = time.perf_counter()
    for x in test_data:
        converted.extend(x.to_bytes(2, "big"))
    et = time.perf_counter()
    print("time {:8f} s - extend blist".format(et - st))
