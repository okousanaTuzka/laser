import time


def neco_time(time_fnc):
    st = time.perf_counter()
    for i in range(10000):
        x = time_fnc()
    now = time.perf_counter()
    print("time: {:14.12f}  func: {}".format(now - st, time_fnc.__name__))


def diff_time(time_fnc):
    no_diff_counter = 0
    last_time = None
    for i in range(10000):
         t = time_fnc()
         if t == last_time:
             no_diff_counter += 1
         last_time = time_fnc()
    print("time: {:6d}  func: {}".format(no_diff_counter, time_fnc.__name__))

def resolution(time_fnc):
    print("resolution: {:.21f} func: {}".format(time.get_clock_info(time_fnc.__name__).resolution, time_fnc.__name__))


def only_increment(time_fnc):
    last_time = 0
    result = True
    for i in range(100):
        t = time_fnc()
        if not t > last_time:
            result = False
            break
        last_time = t
        time.sleep(0.1)
    print("only icrement: {}  func: {}".format(result, time_fnc.__name__))



fncs = [time.time, time.clock, time.monotonic, time.perf_counter, time.process_time]

list(map(resolution, fncs))
list(map(neco_time, fncs))
list(map(diff_time, fncs))
list(map(only_increment, fncs))


