import time
import subprocess

samples = 250000

adc = subprocess.Popen(["sudo", "/home/pi/david/reader", "--sample_rate", "25000"], stdout=subprocess.PIPE, bufsize=100)
stdout = adc.stdout
st = time.time()
for i in range(samples):
    x = int(adc.readline())

print("{}".format(time.time() - st, ))
