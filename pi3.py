import subprocess
import time
from threading import Thread


def read_steam(stream):
    while True:
        try:
            e = stream.readline()
            if e:
                print("Error: " + str(e))
        except Exception as e:
            print(e)


if __name__ == "__main__":

    adc = subprocess.Popen(["sudo", "/home/pi/david/c/core", "--sample_rate", "3000"],
                           stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE, bufsize=100)

    thread = Thread(target=lambda: read_steam(adc.stderr), daemon=True)
    thread.start()
    while True:
        print("send")
        adc.stdin.write((3).to_bytes(4, "little"))
        for i in range(3):
            adc.stdin.write((1000).to_bytes(4, "little"))
        adc.stdin.flush()
        time.sleep(1.5)


